#include "ViewerLoop.h"
#include "ContentCache.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Minigolf
{
	ViewerLoop::ViewerLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch2D& pb) :
		GameLoop(camera, sb, pb),
		model(ContentCache::GetModel("Plane")),
		shader("ModelTint"),
		projection()
	{
		const float fov = glm::half_pi<float>();
		const float width = 800.0f;
		const float height = 600.0f;
		const float nearPlane = 0.1f;
		const float farPlane = 1000.0f;

		projection = glm::perspectiveFov(fov, width, height, nearPlane, farPlane);

		glm::vec3 lightDirection = normalize(glm::vec3(0, -0.25f, -1));
		glm::vec3 lightColor = glm::vec3(1);
		glm::vec3 tint = glm::vec3(1);

		const float ambientIntensity = 0.1f;

		glUseProgram(shader.Program());
		glUniform1f(shader.Uniforms("ambientIntensity"), ambientIntensity);
		glUniform3fv(shader.Uniforms("lightDirection"), 1, value_ptr(lightDirection));
		glUniform3fv(shader.Uniforms("lightColor"), 1, value_ptr(lightColor));
		glUniform3fv(shader.Uniforms("tint"), 1, value_ptr(tint));
	}

	void ViewerLoop::Initialize()
	{
		camera.Position(glm::vec3(0, 2, -6));
		camera.LookAt(glm::vec3(0));
	}

	void ViewerLoop::Update(const float dt)
	{
	}

	void ViewerLoop::Draw()
	{
		const glm::mat3 orientation = glm::mat3(1);
		const glm::mat4 modelM = glm::mat4(1.0f);
		const glm::mat4 mvp = projection * camera.View() * modelM;

		glUseProgram(shader.Program());
		glUniformMatrix4fv(shader.Uniforms("mvp"), 1, false, value_ptr(mvp));
		glUniformMatrix4fv(shader.Uniforms("orientation"), 1, false, value_ptr(orientation));
		glBindVertexArray(model.Vao());
		glActiveTexture(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, model.TextureId());
		glDrawElements(GL_TRIANGLES, model.IndexCount(), GL_UNSIGNED_INT, nullptr);
	}
}
