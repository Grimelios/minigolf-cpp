#pragma once
#include <glm/detail/type_vec2.hpp>
#include "IPositionable2D.h"

namespace Minigolf
{
	class Rectangle : public IPositionable2D
	{
		private:

		float x;
		float y;
		float width;
		float height;

		public:

		Rectangle();
		Rectangle(float width, float height);
		Rectangle(float x, float y, float width, float height);

		float X() const;
		float Y() const;
		float Width() const;
		float Height() const;

		void X(float x);
		void Y(float y);
		void Width(float width);
		void Height(float height);

		glm::vec2 Position() const override;
		glm::vec2 Center() const;

		void Position(glm::vec2 position) override;
		void Center(glm::vec2 center);
	};
}
