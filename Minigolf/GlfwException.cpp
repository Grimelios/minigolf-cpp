#include "GlfwException.h"

namespace Minigolf
{
	GlfwException::GlfwException(const std::string& message) : exception(message.c_str())
	{
	}
}
