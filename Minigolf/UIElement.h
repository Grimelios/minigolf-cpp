#pragma once
#include "Container2D.h"
#include "Enumerations.h"
#include "MouseData.h"

namespace Minigolf
{
	enum class ElementTypes
	{
		Chat
	};

	class UIElement : public Container2D
	{
	private:

		Alignments2D alignment = Alignments2D::Unassigned;

		int offsetX = 0;
		int offsetY = 0;

	public:

		virtual ~UIElement();

		Alignments2D Alignment() const;

		int OffsetX() const;
		int OffsetY() const;

		virtual void HandleMouse(const MouseData& data);

		// Using setters is easier than passing these values down through a constructor.
		void Alignment(Alignments2D alignment);
		void OffsetX(int offsetX);
		void OffsetY(int offsetY);
	};

	inline UIElement::~UIElement() = default;
}
