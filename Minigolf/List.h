#pragma once
#include <vector>
#include "ItemNotFoundException.h"

namespace Minigolf
{
	template<class T>
	class List
	{
		private:

		typedef typename std::vector<T>::iterator iterator;
		typedef const typename std::vector<T>::const_iterator const_iterator;

		std::vector<T> vector;

		public:

		List();
		explicit List(int capacity);
		explicit List(std::vector<T>& vector);

		void Add(const T& item);
		void Add(T&& item);

		template<size_t S>
		void AddRange(const std::array<T, S>& range);
		void AddRange(const std::vector<T>& range);

		void Remove(const T& item);
		void RemoveAt(int index);
		void Clear();

		int Size() const;

		bool IsEmpty() const;

		T& operator[](int index);
		const T& operator[](int index) const;

		iterator begin();
		iterator end();

		const_iterator begin() const;
		const_iterator end() const;
	};

	template <class T>
	List<T>::List() = default;

	template <class T>
	List<T>::List(const int capacity) : vector(capacity)
	{
	}

	template <class T>
	List<T>::List(std::vector<T>& vector) : vector(std::move(vector))
	{
	}

	template <class T>
	void List<T>::Add(const T& item)
	{
		vector.push_back(item);
	}

	template <class T>
	void List<T>::Add(T&& item)
	{
		vector.push_back(std::forward<T>(item));
	}

	template <class T>
	template <size_t S>
	void List<T>::AddRange(const std::array<T, S>& range)
	{
		std::copy(range.begin(), range.end(), std::back_inserter(vector));
	}

	template <class T>
	void List<T>::AddRange(const std::vector<T>& range)
	{
		vector.insert(range.begin(), range.end(), vector.end());
	}

	template <class T>
	void List<T>::Remove(const T& item)
	{
		auto end = vector.end();
		auto value = std::find(vector.begin(), vector.end(), item);

		if (value == end)
		{
			throw ItemNotFoundException("Item not found in the list.");
		}

		vector.erase(value);
	}

	template <class T>
	void List<T>::RemoveAt(const int index)
	{
		vector.erase(vector.begin() + index);
	}

	template <class T>
	void List<T>::Clear()
	{
		vector.clear();
	}

	template <class T>
	int List<T>::Size() const
	{
		return static_cast<int>(vector.size());
	}

	template <class T>
	bool List<T>::IsEmpty() const
	{
		return vector.empty();
	}

	template <class T>
	T& List<T>::operator[](const int index)
	{
		return vector[index];
	}

	template <class T>
	const T& List<T>::operator[](const int index) const
	{
		return vector[index];
	}

	template <class T>
	typename List<T>::iterator List<T>::begin()
	{
		return vector.begin();
	}

	template <class T>
	typename List<T>::iterator List<T>::end()
	{
		return vector.end();
	}

	template <class T>
	typename List<T>::const_iterator List<T>::begin() const
	{
		return vector.begin();
	}

	template <class T>
	typename List<T>::const_iterator List<T>::end() const
	{
		return vector.end();
	}
}
