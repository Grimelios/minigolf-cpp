#pragma once
#include "IPositionable3D.h"
#include "IDynamic.h"
#include <glm/mat4x4.hpp>
#include "Entity.h"

namespace Minigolf
{
	enum class CameraModes
	{
		Fixed,
		Follow,
		Fly
	};

	class Camera : public IPositionable3D, public IDynamic
	{
		private:

		glm::vec3 position;
		glm::vec3 viewTarget;
		glm::mat4 view;

		const Entity* target;
		CameraModes mode;

		void UpdateFollow(float dt);
		void UpdateFly(float dt);

		public:

		Camera();

		glm::vec3 Position() const override;
		const glm::mat4& View() const;

		void Position(glm::vec3 position) override;
		void LookAt(const glm::vec3& position);
		void Target(const Entity& target);
		void Mode(CameraModes mode);
		void Update(float dt) override;
	};
}
