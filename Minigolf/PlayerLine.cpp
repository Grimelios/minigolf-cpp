#include "PlayerLine.h"
#include <glm/detail/type_vec3.hpp>

namespace Minigolf
{
	PlayerLine::PlayerLine(const std::string& playerName) :
		playerName("Default", playerName, true, Alignments2D::Left | Alignments2D::Center)
	{
	}

	void PlayerLine::Color(const glm::vec3& color)
	{
		playerName.Color(glm::vec4(color, 1));
	}

	void PlayerLine::Update(const float dt)
	{
	}

	void PlayerLine::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		playerName.Draw(sb, pb);
	}
}
