#pragma once
#include "Game.h"
#include "GameLoop.h"
#include <memory>
#include "Camera.h"
#include "PrimitiveBatch2D.h"

namespace Minigolf
{
	class MinigolfGame : public Game
	{
	private:

		Camera camera;
		SpriteBatch sb;
		PrimitiveBatch2D pb;

		std::unique_ptr<GameLoop> gameLoop;

	public:

		MinigolfGame();

		void Initialize() override;
		void Update(float dt) override;
		void Draw() override;
	};
}
