#version 330 core

in vec2 fTexCoords;
in vec4 fColor;

uniform sampler2D sampler;

void main()
{
	gl_FragColor = fColor * texture2D(sampler, fTexCoords);
}
