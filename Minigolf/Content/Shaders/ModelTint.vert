#version 330 core

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec2 vTexCoords;

out vec3 fNormal;
out vec2 fTexCoords;

uniform mat4 mvp;
uniform mat3 orientation;

void main()
{
	gl_Position = mvp * vec4(vPosition, 1);

	fNormal = orientation * vNormal;
	fTexCoords = vTexCoords;
}
