#version 330 core

in vec3 fNormal;
in vec2 fTexCoords;

uniform sampler2D sampler;
uniform vec3 tint;
uniform vec3 lightDirection;
uniform vec3 lightColor;
uniform float ambientIntensity;

void main()
{
	vec3 ambient = lightColor * ambientIntensity;
	vec3 diffuse = lightColor * max(dot(fNormal, lightDirection), 0);
	vec3 light = ambient + diffuse;

	gl_FragColor = vec4(tint, 1) * texture2D(sampler, fTexCoords);
}
