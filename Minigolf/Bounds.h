#pragma once
#include <glm/vec2.hpp>
#include "ILocatable.h"

namespace Minigolf
{
	class Bounds : public ILocatable
	{
		private:

		int x;
		int y;
		int width;
		int height;

		public:

		Bounds();
		Bounds(int width, int height);
		Bounds(int x, int y, int width, int height);

		int X() const;
		int Y() const;
		int Width() const;
		int Height() const;
		int Left() const;
		int Right() const;
		int Top() const;
		int Bottom() const;

		void X(int x);
		void Y(int y);
		void Width(int width);
		void Height(int height);
		void Left(int left);
		void Right(int right);
		void Top(int top);
		void Bottom(int bottom);

		glm::ivec2 Location() const override;
		glm::ivec2 Center() const;

		void Location(const glm::ivec2& location) override;
		void Center(const glm::ivec2& center);

		bool Contains(const glm::vec2& point) const;
		bool Contains(const glm::ivec2& point) const;
	};
}
