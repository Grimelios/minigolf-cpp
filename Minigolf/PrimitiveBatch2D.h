#pragma once
#include "PrimitiveGroup2D.h"
#include "ShaderProgram.h"
#include "Bounds.h"
#include "BatchBase.h"
#include "VectorUtilities.h"
#include <array>
#include "SpriteBatch.h"

namespace Minigolf
{
	class SpriteBatch;
	class PrimitiveBatch2D : public BatchBase
	{
	private:

		SpriteBatch* sb = nullptr;
		ShaderProgram shader;
		PrimitiveGroup2D lineGroup;
		PrimitiveGroup2D lineLoopGroup;

		GLuint vao = 0;
		GLuint program = 0;
		GLuint positionBuffer = 0;
		GLuint colorBuffer = 0;
		GLuint indexBuffer = 0;

		std::array<int, 2> lineIndices;
		std::array<int, 4> lineLoopIndices;

		// Note that the index array is intentionally copied so that the index offset can be safely added to each element.
		template<int V, int I>
		void PushData(PrimitiveGroup2D& group, const std::array<glm::vec2, V>& vertexArray, const glm::vec4& color,
			std::array<int, I> indexArray);
		void Flush(PrimitiveGroup2D& group);

	public:

		PrimitiveBatch2D();

		void SetOther(SpriteBatch* sb);
		void DrawLine(const glm::vec2& start, const glm::vec2& end, const glm::vec4& color);
		void DrawBounds(const Bounds& bounds, const glm::vec4& color);
		void Flush() override;
	};

	template<int V, int I>
	void PrimitiveBatch2D::PushData(PrimitiveGroup2D& group, const std::array<glm::vec2, V>& vertexArray,
		const glm::vec4& color, std::array<int, I> indexArray)
	{
		std::vector<glm::vec3> vertices3D(V);

		for (int i = 0; i < V; i++)
		{
			vertices3D[i] = glm::vec3(vertexArray[i], zValue);
		}

		int& indexOffset = group.IndexOffset();

		for (int& index : indexArray)
		{
			index += indexOffset;
		}

		VectorUtilities::PushVector(group.Vertices(), vertices3D);
		VectorUtilities::PushArray(group.Indices(), indexArray);
		VectorUtilities::Repeat(group.Colors(), color, V);

		indexOffset += I;
		zValue -= ZIncrement;
		sb->ZValue(zValue);
	}
}
