#pragma once
#include <string>

namespace Minigolf
{
	class ItemNotFoundException : public std::exception
	{
		public:

		explicit ItemNotFoundException(const std::string& message);
	};
}
