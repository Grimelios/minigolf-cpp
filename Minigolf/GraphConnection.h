#pragma once

namespace Minigolf
{
	template<class T, class E>
	class GraphConnection
	{
		private:

		T& data;
		E& edge;

		public:

		GraphConnection(T& data, E& edge);

		T& Data();
		E& Edge();
	};

	template <class T, class E>
	GraphConnection<T, E>::GraphConnection(T& data, E& edge) : data(data), edge(edge)
	{
	}

	template <class T, class E>
	T& GraphConnection<T, E>::Data()
	{
		return data;
	}

	template <class T, class E>
	E& GraphConnection<T, E>::Edge()
	{
		return edge;
	}
}
