#pragma once
#include "IDynamic.h"
#include "InputData.h"
#include <array>
#include <glfw3.h>
#include <glm/vec2.hpp>
#include "KeyboardData.h"
#include "KeyPress.h"

namespace Minigolf
{
	class InputProcessor : public IDynamic
	{
		private:

		std::array<InputStates, GLFW_KEY_LAST> keyArray;
		std::array<InputStates, GLFW_MOUSE_BUTTON_LAST> buttonArray;
		std::vector<KeyPress> keyPresses;
		std::vector<KeyPress> keyRepeats;

		glm::vec2 mousePosition;
		glm::vec2 previousMousePosition;

		public:

		InputProcessor();

		KeyboardData GetKeyboardData() const;

		void OnKeyPress(int key, int mods);
		void OnKeyRepeat(int key, int mods);
		void OnKeyRelease(int key);
		void OnMouseButtonPress(int button);
		void OnMouseButtonRelease(int button);
		void OnMouseMove(float x, float y);
		void Update(float dt) override;
	};
}
