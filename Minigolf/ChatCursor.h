#pragma once
#include "IDynamic.h"
#include "IRenderable2D.h"
#include "IColorable.h"
#include "Timer.h"

namespace Minigolf
{
	class ChatCursor : public IDynamic, public IRenderable2D
	{
	private:

		glm::ivec2 start = glm::ivec2(0);
		glm::ivec2 end = glm::ivec2(0);
		glm::vec4 color = glm::vec4(1);

		int halfLength = 0;
		int correction = 0;
		int blinkTime = 0;
		int stallTime = 0;
		
		float fadeFraction = 0;

		std::unique_ptr<Timer> blinkTimer;
		std::unique_ptr<Timer> stallTimer;

		Timer::TFunction tick;

		bool fadingOut = true;

	public:

		ChatCursor();

		void Location(const glm::ivec2& location);
		void Color(const glm::vec4& color);
		void Stall();
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
