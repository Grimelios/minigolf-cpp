#pragma once
#include "InputControl.h"

namespace Minigolf
{
	class Checkbox : public InputControl
	{
		private:

		bool checked;

		public:

		bool Checked() const;
		void Checked(bool checked);
	};
}
