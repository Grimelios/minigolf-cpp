#include "Sprite.h"
#include "ContentCache.h"
#include "GameFunctions.h"

namespace Minigolf
{
	Sprite::Sprite(const std::string& texture, const Alignments2D alignment) :
		Sprite(ContentCache::GetTexture(texture), alignment)
	{
	}

	Sprite::Sprite(const Texture2D& texture, const Alignments2D alignment) :
		Component2D(alignment), texture(texture)
	{
		origin = GameFunctions::ComputeOrigin(texture.Width(), texture.Height(), alignment);
	}

	void Sprite::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		sb.Draw(texture, sourceRect, position, origin, rotation, color, scale);
	}
}
