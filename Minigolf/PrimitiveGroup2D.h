#pragma once
#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_vec4.hpp>
#include <vector>
#include <glad.h>

namespace Minigolf
{
	class PrimitiveGroup2D
	{
	private:

		GLenum mode;

		std::vector<glm::vec3> vertices;
		std::vector<glm::vec4> colors;
		std::vector<int> indices;

		int indexOffset = 0;

	public:

		explicit PrimitiveGroup2D(GLenum mode);

		GLenum Mode() const;

		std::vector<glm::vec3>& Vertices();
		std::vector<glm::vec4>& Colors();
		std::vector<int>& Indices();

		int& IndexOffset();
		bool IsEmpty() const;
		void Clear();
	};
}
