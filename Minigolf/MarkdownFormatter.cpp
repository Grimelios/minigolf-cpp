#include "MarkdownTag.h"
#include "VectorUtilities.h"
#include "MarkdownFormatter.h"
#include "ArrayUtilities.h"

namespace Minigolf
{
	MarkdownFormatter::MarkdownFormatter() :
		markdownArray({	'*', '_', '~' })
	{
	}

	std::vector<MarkdownToken> MarkdownFormatter::ComputeTokens(const std::string& value) const
	{
		std::vector<MarkdownTag> openTags;
		std::vector<MarkdownTag> closedTags;

		int lastClose = 0;
		const int length = static_cast<int>(value.size());

		for (int i = 0; i < length; i++)
		{
			char c = value[i];

			// Markdown characters include * (italics and bold), _ (underline), and ~ (strikethrough).
			if (ArrayUtilities::Contains(markdownArray, c))
			{
				int tagSize = 0;
				bool closed = false;

				MarkdownTypes type = Advance(value, c, i, tagSize);

				for (int j = 0; j < static_cast<int>(openTags.size()); j++)
				{
					MarkdownTag& tag = openTags[j];

					// A bitwise comparison is used rather than direct equality because asterisks are overloaded and
					// can represent tags of different lengths.
					const int comparison = static_cast<int>(tag.Type()) & static_cast<int>(type);

					if (comparison > 0)
					{
						const int start = tag.Start();
						const int end = i - 1;

						// Regular tokens are inserted in between other font types as appropriate.
						if (start - tagSize > lastClose)
						{
							closedTags.emplace_back(lastClose, start - tagSize - 1, MarkdownTypes::Regular);
						}

						closedTags.emplace_back(start, end, type);
						closed = true;

						// Note that "last close" is actually one character past the last closing tag.
						lastClose = end + tagSize + 1;

						VectorUtilities::RemoveFrom(openTags, j);

						break;
					}
				}

				if (!closed)
				{
					openTags.emplace_back(i + tagSize, type);
				}

				i += tagSize - 1;
			}
		}

		std::vector<MarkdownToken> tokens;

		// This covers a message with no markdown whatsoever (which will likely be by far the most common use case).
		if (closedTags.empty())
		{
			tokens.emplace_back(value, MarkdownTypes::Regular);
		}
		else
		{
			// This covers any leftover regular text at the end of a message.
			if (lastClose < length)
			{
				closedTags.emplace_back(lastClose, length - 1, MarkdownTypes::Regular);
			}

			for (MarkdownTag& tag : closedTags)
			{
				const int start = tag.Start();
				const int end = tag.End();

				tokens.emplace_back(value.substr(start, end - start + 1), tag.Type());
			}
		}

		return tokens;
	}

	MarkdownTypes MarkdownFormatter::Advance(const std::string& s, const char c, const int index, int& tagSize)
	{
		tagSize = 1;

		switch (c)
		{
			case '_': return MarkdownTypes::Underline;
			case '~': return MarkdownTypes::Strikethrough;
		}

		while (tagSize < 3 && index < static_cast<int>(s.size()) - tagSize && s[index + tagSize] == '*')
		{
			tagSize++;
		}
		
		switch (tagSize)
		{
			case 1: return MarkdownTypes::Italic;
			case 2: return MarkdownTypes::Bold;
		}

		return MarkdownTypes::BoldItalic;
	}
}
