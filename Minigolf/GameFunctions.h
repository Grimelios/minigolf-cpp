#pragma once
#include "Enumerations.h"
#include <glm/vec2.hpp>
#include <array>

namespace Minigolf
{
	namespace GameFunctions
	{
		glm::ivec2 ComputeOrigin(int width, int height, Alignments2D alignment);

		template<class T, size_t S>
		void PositionItems(std::array<T, S>& items, const glm::vec2& basePosition, const glm::vec2& spacing)
		{
			for (unsigned int i = 0; i < items.size(); i++)
			{
				items[i].Position(basePosition + spacing * static_cast<float>(i));
			}
		}
	}
}
