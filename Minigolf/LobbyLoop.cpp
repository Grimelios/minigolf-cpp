#include "LobbyLoop.h"
#include "GameFunctions.h"
#include "Messaging.h"
#include "TextboxChatRenderer.h"

namespace Minigolf
{
	LobbyLoop::LobbyLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch2D& pb) : GameLoop(camera, sb, pb),
		textArray(
		{
			SpriteText("Chat", "Example string using regular text.", false),
			SpriteText("ChatItalic", "Example string using italic text.", false),
			SpriteText("ChatBold", "Example string using bold text.", false),
			SpriteText("ChatBoldItalic", "Example string using bold-italic text.", false),
		})
	{
		Messaging::Subscribe(MessageTypes::Keyboard, [this](void* data, const float dt)
		{
			HandleKeyboard(*static_cast<KeyboardData*>(data));
		});
	}

	void LobbyLoop::Initialize()
	{
		GameFunctions::PositionItems(textArray, glm::vec2(20), glm::vec2(0, 24));

		chatElement.Location(glm::ivec2(20, 150));
	}

	void LobbyLoop::HandleKeyboard(const KeyboardData& data)
	{
		chatElement.HandleKeyboard(data);
	}

	void LobbyLoop::Update(const float dt)
	{
		chatElement.Update(dt);
	}

	void LobbyLoop::Draw()
	{
		for (SpriteText& text : textArray)
		{
			text.Draw(sb, pb);
		}

		chatElement.Draw(sb, pb);
	}
}
