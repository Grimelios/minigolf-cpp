#include "MenuItem.h"

namespace Minigolf
{
	MenuItem::MenuItem(const std::string& value, const int index, const Menu* parent) :
		spriteText("Default", value), parent(parent), index(index)
	{
	}

	void MenuItem::OnSelect()
	{
	}

	void MenuItem::OnDeselect()
	{
	}

	void MenuItem::OnSubmit()
	{
	}

	void MenuItem::OnHover()
	{
	}

	void MenuItem::OnUnhover()
	{
	}

	void MenuItem::OnClick()
	{
	}

	bool MenuItem::Contains(const glm::vec2& mousePosition)
	{
		return false;
	}

	void MenuItem::Update(const float dt)
	{
	}

	void MenuItem::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		spriteText.Draw(sb, pb);
	}
}
