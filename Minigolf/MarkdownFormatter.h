#pragma once
#include "MarkdownTag.h"
#include "MarkdownToken.h"
#include <array>

namespace Minigolf
{
	class MarkdownFormatter
	{
		private:

		std::array<char, 3> markdownArray;

		static MarkdownTypes Advance(const std::string& s, char c, int index, int& tagSize);

		public:

		MarkdownFormatter();

		std::vector<MarkdownToken> ComputeTokens(const std::string& value) const;
	};
}
