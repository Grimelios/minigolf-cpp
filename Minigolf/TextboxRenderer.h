#pragma once
#include "IRenderable2D.h"
#include "IDynamic.h"
#include "Textbox.h"

namespace Minigolf
{
	class Textbox;
	class TextboxRenderer : public IDynamic, public IRenderable2D
	{
	public:

		virtual ~TextboxRenderer();
		virtual void Location(const glm::ivec2& location) = 0;
		virtual void Clear() = 0;
		virtual void OnValue(const std::string& value) = 0;
		virtual void OnModify(int cursor, int removalCount, const std::string& s) = 0;
		virtual void OnCursor(int cursor) = 0;
		virtual void OnInsert(bool insert) = 0;
		virtual void OnHover() = 0;
		virtual void OnUnhover() = 0;
		virtual void OnClick() = 0;
	};

	inline TextboxRenderer::~TextboxRenderer() = default;
}
