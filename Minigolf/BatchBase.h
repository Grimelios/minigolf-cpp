#pragma once

namespace Minigolf
{
	// This class exists so that both of the 2D batch classes (SpriteBatch and PrimitiveBatch2D) can draw in any order and
	// maintain consistent Z-sorting.
	class BatchBase
	{
	protected:

		// Z-sorting is implemented using 3D coordinates. For each draw call, the current Z value is decremented by a small
		// amount, then reset on flush. Note that the Z value is only reset to its maximum value once at the beginning of
		// each frame (since sprites should continue to be sorted even through batch restarts).
		const float ZStart = 0.999f;
		const float ZIncrement = 0.00001f;

		float zValue = ZStart;

	public:

		virtual ~BatchBase() = 0;
		virtual void Flush() = 0;

		void ZValue(float zValue);
	};

	inline BatchBase::~BatchBase() = default;
}
