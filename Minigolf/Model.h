#pragma once
#include <glad.h>
#include <vector>
#include <glm/detail/type_vec3.hpp>

namespace Minigolf
{
	class Model
	{
		private:

		GLuint vao = 0;
		GLuint textureId = 0;

		unsigned int indexCount;

		public:

		Model(const std::vector<glm::vec3>& vertices, const std::vector<glm::vec3>& normals,
			const std::vector<glm::vec2>& texCoords, const std::vector<unsigned int>& indices, GLuint textureId);

		GLuint Vao() const;
		GLuint TextureId() const;

		unsigned int IndexCount() const;
	};
}
