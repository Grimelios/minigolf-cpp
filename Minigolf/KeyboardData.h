#pragma once
#include "InputData.h"
#include <glfw3.h>
#include <array>
#include <vector>
#include "KeyPress.h"

namespace Minigolf
{
	class KeyboardData : public InputData
	{
		private:

		const std::vector<int> keysDown;
		const std::vector<KeyPress> keysPressedThisFrame;
		const std::vector<int> keysReleasedThisFrame;
		const std::array<InputStates, GLFW_KEY_LAST>& keyArray;

		public:

		KeyboardData(std::vector<int> keysDown, std::vector<KeyPress> keysPressedThisFrame, std::vector<int> keysReleasedThisFrame,
			const std::array<InputStates, GLFW_KEY_LAST>& keyArray);

		const std::vector<int>& KeysDown() const;
		const std::vector<KeyPress>& KeysPressedThisFrame() const;
		const std::vector<int>& KeysReleasedThisFrame() const;

		bool Query(void* data, InputStates state) const override;
	};
}
