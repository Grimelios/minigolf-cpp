#include "TextureLoader.h"
#include <glad.h>
#include <vector>
#include <lodepng.h>

namespace Minigolf
{
	std::unique_ptr<Texture2D> TextureLoader::Load(const std::string& filename) const
	{
		GLuint textureId;

		std::vector<unsigned char> buffer;
		unsigned int width;
		unsigned int height;

		// The texture folder is included with the filename.
		lodepng::decode(buffer, width, height, filename);

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &buffer[0]);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glGenerateMipmap(GL_TEXTURE_2D);

		return std::make_unique<Texture2D>(Texture2D(textureId, width, height));
	}
}
