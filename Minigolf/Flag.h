#pragma once
#include "Entity.h"

namespace Minigolf
{
	enum class FlagTypes
	{
		Rectangle,
		Triangle,
		Banner
	};

	class Flag : public Entity
	{
		public:

		Flag(FlagTypes flagType, int width, int height, const std::string& texture);
	};
}
