#pragma once
#include <map>
#include <string>
#include "Rectangle.h"

namespace Minigolf
{
	class TextureAtlas
	{
		private:

		std::map<std::string, Rectangle> map;

		public:

		const Rectangle& operator[](const std::string& key);
	};
}
