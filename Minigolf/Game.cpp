#include <glad.h>
#include "Game.h"
#include "GlfwException.h"
#include "GladException.h"
#include "Messaging.h"

namespace Minigolf
{
	const float Game::TargetFramerate = 1.0f / 60;

	Game::Game(const std::string& title) 
	{
		// Initialize GLFW.
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		// Create the game window.
		window = glfwCreateWindow(DefaultWidth, DefaultHeight, title.c_str(), nullptr, nullptr);

		if (window == nullptr)
		{
			glfwTerminate();

			throw GlfwException(std::string("Window creation failed."));
		}

		glfwMakeContextCurrent(window);
		glfwSetWindowUserPointer(window, this);

		// Initialize GLAD.
		if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
		{
			glfwTerminate();

			throw GladException(std::string("GLAD initialization failed."));
		}

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glViewport(0, 0, 800, 600);

		RegisterCallbacks();
	}

	void Game::RegisterCallbacks() const
	{
		glfwSetKeyCallback(window, KeyCallback);
		glfwSetCharCallback(window, CharCallback);
		glfwSetCursorPosCallback(window, CursorCallback);
		glfwSetMouseButtonCallback(window, ButtonCallback);
		glfwSetFramebufferSizeCallback(window, ResizeCallback);
	}

	void Game::KeyCallback(GLFWwindow* window, const int key, const int scancode, const int action, const int mods)
	{
		InputProcessor& processor = static_cast<Game*>(glfwGetWindowUserPointer(window))->GetInputProcessor();
		
		switch (action)
		{
			case GLFW_PRESS: processor.OnKeyPress(key, mods); break;
			case GLFW_REPEAT: processor.OnKeyRepeat(key, mods); break;
			case GLFW_RELEASE: processor.OnKeyRelease(key); break;
		}
	}

	void Game::CharCallback(GLFWwindow* window, unsigned int codepoint)
	{
	}

	void Game::CursorCallback(GLFWwindow* window, const double x, const double y)
	{
		const float fX = static_cast<float>(x);
		const float fY = static_cast<float>(y);

		InputProcessor& processor = static_cast<Game*>(glfwGetWindowUserPointer(window))->GetInputProcessor();
		processor.OnMouseMove(fX, fY);
	}

	void Game::ButtonCallback(GLFWwindow* window, const int button, const int action, int mods)
	{
		InputProcessor& processor = static_cast<Game*>(glfwGetWindowUserPointer(window))->GetInputProcessor();
		
		switch (action)
		{
			case GLFW_PRESS: processor.OnMouseButtonPress(button);
			case GLFW_RELEASE: processor.OnMouseButtonRelease(button);
		}
	}

	void Game::ResizeCallback(GLFWwindow* window, const int width, const int height)
	{
		glViewport(0, 0, width, height);

		glm::ivec2 dimensions = glm::ivec2(width, height);

		Messaging::Send(MessageTypes::Resize, &dimensions);
	}

	InputProcessor& Game::GetInputProcessor()
	{
		return inputProcessor;
	}

	void Game::Run()
	{
		while (!glfwWindowShouldClose(window))
		{
			const float time = static_cast<float>(glfwGetTime());
			
			// Using an accumulator helps limit FPS to the desired target.
			accumulator += time - previousTime;
			previousTime = time;

			const bool shouldUpdate = accumulator >= TargetFramerate;

			if (shouldUpdate)
			{
				glfwPollEvents();

				while (accumulator >= TargetFramerate)
				{
					Update(TargetFramerate);

					accumulator -= TargetFramerate;
				}

				Draw();

				glfwSwapBuffers(window);
			}

		}

		glfwTerminate();
	}
}
