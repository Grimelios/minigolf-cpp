#pragma once
#include <glm/gtc/quaternion.hpp>

namespace Minigolf
{
	class IOrientable
	{
		public:

		virtual ~IOrientable();
		virtual glm::quat Orientation() const = 0;
		virtual void Orientation(const glm::quat& orientation) = 0;
	};

	inline IOrientable::~IOrientable() = default;
}
