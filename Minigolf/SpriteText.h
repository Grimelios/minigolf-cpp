#pragma once
#include "Component2D.h"
#include "Enumerations.h"
#include "SpriteFont.h"
#include <string>
#include <optional>

namespace Minigolf
{
	class SpriteText : public Component2D
	{
	private:

		const SpriteFont& font;

		std::optional<std::string> value;

	public:

		explicit SpriteText(const std::string& font, const std::optional<std::string>&& value = std::nullopt, bool useKerning = true,
			Alignments2D alignment = Alignments2D::Left | Alignments2D::Top);
		explicit SpriteText(const SpriteFont& font, const std::optional<std::string>& value = std::nullopt,
			Alignments2D alignment = Alignments2D::Left | Alignments2D::Top);

		glm::ivec2 Measure() const;

		void Value(const std::optional<std::string>& value);
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
