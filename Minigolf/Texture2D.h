#pragma once
#include <glad.h>

namespace Minigolf
{
	class Texture2D
	{
		private:

		int width = -1;
		int height = -1;

		GLuint textureId;

		public:

		Texture2D(GLuint id, int width, int height);

		int Width() const;
		int Height() const;

		GLuint TextureId() const;
	};
}
