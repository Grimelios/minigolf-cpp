#pragma once
#include <string>
#include "List.h"
#include "HoleData.h"

namespace Minigolf
{
	class CourseData
	{
		private:

		std::string name;

		// This variable is a list rather than an array to accomodate both 9- and 18-hole courses.
		List<HoleData> holeList;

		public:

		CourseData(std::string name);

		const std::string& Name() const;
	};
}
