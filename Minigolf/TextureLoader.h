#pragma once

#include "Texture2D.h"
#include <memory>
#include "GenericLoader.h"

namespace Minigolf
{
	class TextureLoader : public GenericLoader<Texture2D>
	{
		public:

		std::unique_ptr<Texture2D> Load(const std::string& filename) const override;
	};
}
