#include "StringUtilities.h"
#include <sstream>
#include "SpriteFont.h"

namespace Minigolf
{
	std::vector<std::string> StringUtilities::Split(const std::string& s, const char delimeter, const bool removeEmpty)
	{
		std::stringstream stream(s);
		std::string token;
		std::vector<std::string> list;

		while (std::getline(stream, token, delimeter))
		{
			if (!removeEmpty || !token.empty())
			{
				list.push_back(std::move(token));
			}
		}

		return list;
	}

	std::vector<std::string> StringUtilities::WrapLines(const SpriteFont& font, const std::string& s, const int targetWidth,
		const int start)
	{
		// Removing empty whitespace (spaces) makes wrapping calculations much easier.
		std::vector<std::string> words = Split(s, ' ', true);
		std::vector<std::string> lines;
		std::string currentLine;

		int currentWidth = start;
		const int spaceWidth = font.Measure(" ").x;

		for (int i = 0; i < static_cast<int>(words.size()); i++)
		{
			const std::string& word = words[i];
			const int wordWidth = font.Measure(word).x;

			if (currentWidth + spaceWidth + wordWidth > targetWidth)
			{
				lines.push_back(std::move(currentLine));
				currentLine = word;
				currentWidth = wordWidth;
			}
			else if (i == 0)
			{
				currentLine = word;
				currentWidth = wordWidth;
			}
			else
			{
				currentLine += " " + word;
				currentWidth += spaceWidth + wordWidth;
			}
		}

		if (s[s.size() - 1] == ' ')
		{
			currentLine.push_back(' ');
		}

		if (!currentLine.empty())
		{
			lines.push_back(std::move(currentLine));
		}

		return lines;
	}

	std::string StringUtilities::RemoveExtension(const std::string& s)
	{
		const int index = static_cast<int>(s.find_last_of('.'));

		return s.substr(0, index);
	}

	int StringUtilities::IndexOf(const std::string& s, const char c)
	{
		return static_cast<int>(s.find(c));
	}

	int StringUtilities::IndexOf(const std::string& s, const char c, const int start)
	{
		return static_cast<int>(s.find(c, start));
	}

	int StringUtilities::IndexOf(const std::string& s, const std::string& value)
	{
		return static_cast<int>(s.find(value));
	}

	int StringUtilities::IndexOf(const std::string& s, const std::string& value, const int start)
	{
		return static_cast<int>(s.find(value, start));
	}

	int StringUtilities::LastIndexOf(const std::string& s, const char c)
	{
		return static_cast<int>(s.find_last_of(c));
	}

	void StringUtilities::ReplaceAt(std::string& s, const std::string& value, const int index)
	{
		for (unsigned int i = 0; i < value.size(); i++)
		{
			s[index + i] = value[i];
		}
	}
}
