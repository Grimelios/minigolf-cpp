#include "SpriteGroup.h"

namespace Minigolf
{
	std::vector<glm::vec3>& SpriteGroup::Vertices()
	{
		return vertices;
	}

	std::vector<glm::vec2>& SpriteGroup::TexCoords()
	{
		return texCoords;
	}

	std::vector<glm::vec2>& SpriteGroup::QuadCoords()
	{
		return quadCoords;
	}

	std::vector<glm::vec4>& SpriteGroup::Colors()
	{
		return colors;
	}

	std::vector<int>& SpriteGroup::Indices()
	{
		return indices;
	}

	bool SpriteGroup::IsEmpty() const
	{
		return indices.empty();
	}

	int& SpriteGroup::IndexOffset()
	{
		return indexOffset;
	}

	void SpriteGroup::Clear()
	{
		vertices.clear();
		texCoords.clear();
		quadCoords.clear();
		colors.clear();
		indices.clear();

		indexOffset = 0;
	}
}
