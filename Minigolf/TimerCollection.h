#pragma once
#include "Timer.h"
#include "SafeList.h"

namespace Minigolf
{
	class Timer;
	class TimerCollection : public IDynamic
	{
	private:

		SafeList<Timer> timers;

	public:

		void Add(Timer& timer);
		void Remove(const Timer& timer);
		void Update(float dt) override;
	};
}
