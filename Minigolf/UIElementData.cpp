#include "UIElementData.h"

namespace Minigolf
{
	UIElementData::UIElementData(const Json& j) :
		alignment(j.at("Alignment").get<Alignments2D>()),
		offsetX(j.at("OffsetX").get<int>()),
		offsetY(j.at("OffsetY").get<int>()),
		type(j.at("Type").get<std::string>())
	{
	}

	Alignments2D UIElementData::Alignment() const
	{
		return alignment;
	}

	int UIElementData::OffsetX() const
	{
		return offsetX;
	}

	int UIElementData::OffsetY() const
	{
		return offsetY;
	}

	const std::string& UIElementData::Type() const
	{
		return type;
	}
}
