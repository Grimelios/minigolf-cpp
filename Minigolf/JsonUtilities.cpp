#include "JsonUtilities.h"
#include <nlohmann/json.hpp>
#include <fstream>
#include "Paths.h"

namespace Minigolf
{
	Json JsonUtilities::Deserialize(const std::string& filename)
	{
		std::ifstream stream(Paths::Json + filename);
		Json j;
		stream >> j;

		return j;
	}
}
