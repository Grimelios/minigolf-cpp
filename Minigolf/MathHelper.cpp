#include "MathHelper.h"

namespace Minigolf
{
	float MathHelper::Lerp(const float f1, const float f2, const float amount)
	{
		return f1 + (f2 - f1) * amount;
	}
}
