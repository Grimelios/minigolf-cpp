#include "Bounds.h"

namespace Minigolf
{
	Bounds::Bounds() : Bounds(0, 0, 0, 0)
	{
	}

	Bounds::Bounds(const int width, const int height) : Bounds(0, 0, width, height)
	{
	}

	Bounds::Bounds(const int x, const int y, const int width, const int height) :
		x(x), y(y), width(width), height(height)
	{
	}

	int Bounds::X() const
	{
		return x;
	}

	int Bounds::Y() const
	{
		return y;
	}

	int Bounds::Width() const
	{
		return width;
	}

	int Bounds::Height() const
	{
		return height;
	}

	int Bounds::Left() const
	{
		return x;
	}

	int Bounds::Right() const
	{
		return x + width - 1;
	}

	int Bounds::Top() const
	{
		return y;
	}

	int Bounds::Bottom() const
	{
		return y + height - 1;
	}

	void Bounds::X(const int x)
	{
		this->x = x;
	}

	void Bounds::Y(const int y)
	{
		this->y = y;
	}

	void Bounds::Width(const int width)
	{
		this->width = width;
	}

	void Bounds::Height(const int height)
	{
		this->height = height;
	}

	void Bounds::Left(const int left)
	{
		x = left;
	}

	void Bounds::Right(const int right)
	{
		x = right - width + 1;
	}

	void Bounds::Top(const int top)
	{
		y = top;
	}

	void Bounds::Bottom(const int bottom)
	{
		y = bottom - height + 1;
	}

	glm::ivec2 Bounds::Location() const
	{
		return glm::ivec2(x, y);
	}

	glm::ivec2 Bounds::Center() const
	{
		return glm::ivec2(x + width / 2, y + height / 2);
	}

	void Bounds::Location(const glm::ivec2& location)
	{
		x = location.x;
		y = location.y;
	}

	void Bounds::Center(const glm::ivec2& center)
	{
		x = center.x - width / 2;
		y = center.y - height / 2;
	}

	bool Bounds::Contains(const glm::vec2& point) const
	{
		const float pX = point.x;
		const float pY = point.y;

		return pX >= x && pX <= Right() && pY >= y && pY <= Bottom();
	}

	bool Bounds::Contains(const glm::ivec2& point) const
	{
		const int pX = point.x;
		const int pY = point.y;

		return pX >= x && pX <= Right() && pY >= y && pY <= Bottom();
	}
}
