#pragma once
#include "GameLoop.h"
#include "SpriteText.h"
#include "Textbox.h"
#include "ChatElement.h"

namespace Minigolf
{
	class LobbyLoop : public GameLoop
	{
		private:

		ChatElement chatElement;
		std::array<SpriteText, 4> textArray;

		public:

		LobbyLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch2D& pb);

		void Initialize() override;
		void HandleKeyboard(const KeyboardData& data);
		void Update(float dt) override;
		void Draw() override;
	};
}
