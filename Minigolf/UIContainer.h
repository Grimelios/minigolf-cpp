#pragma once
#include "IDynamic.h"
#include "IRenderable2D.h"
#include "UIElement.h"
#include <memory>
#include <vector>
#include "UIElementData.h"

namespace Minigolf
{
	class UIContainer : public IDynamic, public IRenderable2D
	{
	private:

		using TypeMap = std::map<std::string, ElementTypes>;

		static TypeMap typeMap;
		static std::unique_ptr<UIElement> CreateElement(const UIElementData& data);

		std::vector<std::unique_ptr<UIElement>> elements;

		void PositionElements();

	protected:

		explicit UIContainer(const std::string& filename);

	public:

		virtual ~UIContainer() = 0;

		UIElement& GetElement();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};

	inline UIContainer::~UIContainer() = default;
}
