#include "MarkdownToken.h"
#include <utility>

namespace Minigolf
{
	MarkdownToken::MarkdownToken(std::string value, const MarkdownTypes type) :
		value(std::move(value)),
		type(type)
	{
	}

	const std::string& MarkdownToken::Value() const
	{
		return value;
	}

	MarkdownTypes MarkdownToken::Type() const
	{
		return type;
	}
}
