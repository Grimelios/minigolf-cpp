#include "GolfBall.h"
#include "ContentCache.h"

namespace Minigolf
{
	GolfBall::GolfBall() : model(ContentCache::GetModel("Pyramid.obj"))
	{
	}

	const Model& GolfBall::Model() const
	{
		return model;
	}

	void GolfBall::Update(const float dt)
	{
		rotation += 0.001f;
		orientation = glm::quat(glm::vec3(rotation, rotation, 0));
	}
}
