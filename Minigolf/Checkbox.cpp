#include "Checkbox.h"

namespace Minigolf
{
	bool Checkbox::Checked() const
	{
		return checked;
	}

	void Checkbox::Checked(const bool checked)
	{
		this->checked = checked;
	}
}
