#pragma once
#include "InputData.h"
#include <array>
#include <glfw3.h>
#include <glm/detail/type_vec2.hpp>

namespace Minigolf
{
	enum class MouseButtons
	{
		Left,
		Right,
		Middle
	};

	class MouseData : public InputData
	{
		private:

		std::array<InputStates, GLFW_MOUSE_BUTTON_LAST> buttonArray;

		glm::vec2 position;
		glm::vec2 previousPosition;

		public:

		MouseData(glm::vec2 position, glm::vec2 previousPosition,
			const std::array<InputStates, GLFW_MOUSE_BUTTON_LAST>& buttonArray);

		const glm::vec2& Position() const;
		const glm::vec2& PreviousPosition() const;

		bool Query(void* data, InputStates state) const override;
	};
}
