#pragma once
#include "InputData.h"

namespace Minigolf
{
	class InputBind
	{
		private:

		InputTypes type;

		void* data;

		public:

		InputBind(InputTypes type, void* data);

		InputTypes Type() const;

		void* Data() const;
		void Type(InputTypes type);
		void Data(void* data);
	};
}
