#pragma once

#include "IPositionable2D.h"
#include "IRotatable.h"
#include "IRenderable2D.h"
#include "IColorable.h"
#include "IScalable2D.h"
#include "Enumerations.h"

namespace Minigolf
{
	class Component2D : public IPositionable2D, public IRotatable, public IScalable2D, public IColorable, public IRenderable2D
	{
		protected:

		glm::vec2 position = glm::vec2(0.0f);
		glm::vec2 scale = glm::vec2(1.0f);
		glm::ivec2 origin = glm::ivec2(0);
		glm::vec4 color = glm::vec4(1.0f);

		float rotation = 0;

		Alignments2D alignment = Alignments2D::Unassigned;

		explicit Component2D(Alignments2D alignment);
		virtual ~Component2D() = 0;

		public:

		glm::vec2 Position() const override;
		glm::vec2 Scale() const override;
		glm::ivec2 Origin() const;
		glm::vec4 Color() const override;

		float Rotation() const override;

		void Position(glm::vec2 position) override;
		void Scale(glm::vec2 scale) override;
		void Origin(glm::ivec2 origin);
		void Color(const glm::vec4& color) override;
		void Rotation(float rotation) override;
	};

	inline Component2D::~Component2D() = default;
}
