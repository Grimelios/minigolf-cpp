#pragma once
#include <string>
#include <nlohmann/json.hpp>
#include "CommonAliases.h"

namespace Minigolf::JsonUtilities
{
	Json Deserialize(const std::string& filename);
}
