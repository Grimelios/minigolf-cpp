#include "Properties.h"
#include <vector>
#include "MapUtilities.h"
#include "FileUtilities.h"
#include "Paths.h"
#include "StringUtilities.h"

namespace Minigolf
{
	Properties::PropertyCache Properties::cache;

	Properties::PropertyMap& Properties::Load(const std::string& filename)
	{
		PropertyMap* existingMap;

		if (MapUtilities::TryGetValue(cache, filename, existingMap))
		{
			return *existingMap;
		}

		PropertyMap map;
		std::vector<std::string> lines = FileUtilities::ReadAllLines(Paths::Properties + filename);

		for (std::string& line : lines)
		{
			if (line.empty())
			{
				continue;
			}

			// Each property line is in the form "key=value".
			std::vector<std::string> tokens = StringUtilities::Split(line, '=');
			MapUtilities::Add(map, tokens[0], tokens[1]);
		}

		MapUtilities::Add(cache, filename, map);

		return MapUtilities::Get(cache, filename);
	}
}
