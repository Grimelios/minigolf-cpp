#pragma once
#include "UIElement.h"

namespace Minigolf
{
	
	class CourseDisplay : public UIElement
	{
		public:

		CourseDisplay();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
