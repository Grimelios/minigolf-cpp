#include "CourseData.h"

namespace Minigolf
{
	CourseData::CourseData(std::string name) : name(std::move(name))
	{
	}

	const std::string& CourseData::Name() const
	{
		return name;
	}
}
