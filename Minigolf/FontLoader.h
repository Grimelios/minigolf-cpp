#pragma once
#include "SpriteFont.h"
#include <string>
#include <map>

namespace Minigolf
{
	class FontLoader
	{
		private:

		int ParseValue(const std::string& s) const;
		void ParseKerning();

		public:

		const SpriteFont Load(const std::string& name, bool useKerning = true) const;
	};
}
