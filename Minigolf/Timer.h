#pragma once
#include "IDynamic.h"
#include "TimerCollection.h"
#include <functional>

namespace Minigolf
{
	class TimerCollection;
	class Timer : public IDynamic
	{
	public:

		// N, R, and T stand for non-repeating, repeating, and tick.
		using NFunction = std::function<void(float)>;
		using RFunction = std::function<bool(float)>;
		using TFunction = std::function<void(float)>;

	private:

		TimerCollection* collection = nullptr;

		float elapsed;
		float duration;

		bool paused = false;
		bool repeating;

		NFunction nFunction;
		RFunction rFunction;
		TFunction* tFunction = nullptr;

	public:

		Timer(int duration, const NFunction& function, float elapsed = 0);
		Timer(int duration, const RFunction& function, float elapsed = 0);
		Timer(float duration, NFunction function, float elapsed = 0);
		Timer(float duration, RFunction function, float elapsed = 0);

		bool Paused() const;

		void Paused(bool paused);
		void Tick(TFunction* tFunction);
		void Reset();
		void SetTimerCollection(TimerCollection* collection);
		void Update(float dt) override;

		bool operator==(const Timer& other) const;
	};
}
