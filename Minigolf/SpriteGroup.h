#pragma once
#include <vector>
#include <glm/detail/type_vec2.hpp>
#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_vec4.hpp>

namespace Minigolf
{
	class SpriteGroup
	{
		private:

		std::vector<glm::vec3> vertices;
		std::vector<glm::vec2> texCoords;
		std::vector<glm::vec2> quadCoords;
		std::vector<glm::vec4> colors;
		std::vector<int> indices;

		int indexOffset = 0;

		public:

		std::vector<glm::vec3>& Vertices();
		std::vector<glm::vec2>& TexCoords();
		std::vector<glm::vec2>& QuadCoords();
		std::vector<glm::vec4>& Colors();
		std::vector<int>& Indices();

		bool IsEmpty() const;
		int& IndexOffset();
		void Clear();
	};
}
