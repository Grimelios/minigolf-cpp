#pragma once

#include "SpriteBatch.h"
#include "PrimitiveBatch2D.h"

namespace Minigolf
{
	class IRenderable2D
	{
		public:

		virtual ~IRenderable2D();
		virtual void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) = 0;
	};

	inline IRenderable2D::~IRenderable2D() = default;
}
