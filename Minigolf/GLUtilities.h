#pragma once
#include "List.h"

namespace Minigolf {
	class Model;
}

namespace Minigolf
{
	namespace GLUtilities
	{
		template<class T>
		void BufferData(const GLuint buffer, std::vector<T> data, const int stride)
		{
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			glBufferData(GL_ARRAY_BUFFER, data.size() * stride, &data[0], GL_STATIC_DRAW);
		}

		template<class T>
		void InitializeBuffer(const GLuint buffer, const int index, std::vector<T> data, const GLenum type,
			const int size,	const int stride)
		{
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			glBufferData(GL_ARRAY_BUFFER, data.size() * stride, &data[0], GL_STATIC_DRAW);
			glVertexAttribPointer(index, size, type, GL_FALSE, stride, nullptr);
			glEnableVertexAttribArray(index);
		}
	}
}
