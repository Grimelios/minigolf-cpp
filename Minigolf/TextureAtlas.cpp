#include "TextureAtlas.h"

namespace Minigolf
{
	const Rectangle& TextureAtlas::operator[](const std::string& key)
	{
		return map.find(key)->second;
	}
}
