#pragma once
#include <array>
#include "InputData.h"
#include "List.h"
#include "InputBind.h"

namespace Minigolf
{
	class AggregateData
	{
		private:

		std::array<const InputData*, static_cast<int>(InputTypes::Count)> dataArray;

		public:

		AggregateData();

		const InputData& Data(InputTypes type) const;
		void Data(InputTypes type, const InputData& data);

		bool Query(const List<InputBind>& binds, InputStates state) const;
	};
}
