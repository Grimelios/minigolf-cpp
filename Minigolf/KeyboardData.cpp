#include "KeyboardData.h"

namespace Minigolf
{
	KeyboardData::KeyboardData(std::vector<int> keysDown, std::vector<KeyPress> keysPressedThisFrame,
		std::vector<int> keysReleasedThisFrame, const std::array<InputStates, 348>& keyArray) :
		keysDown(std::move(keysDown)), keysPressedThisFrame(std::move(keysPressedThisFrame)),
		keysReleasedThisFrame(std::move(keysReleasedThisFrame)), keyArray(keyArray)
	{
	}

	const std::vector<int>& KeyboardData::KeysDown() const
	{
		return keysDown;
	}

	const std::vector<KeyPress>& KeyboardData::KeysPressedThisFrame() const
	{
		return keysPressedThisFrame;
	}

	const std::vector<int>& KeyboardData::KeysReleasedThisFrame() const
	{
		return keysReleasedThisFrame;
	}

	bool KeyboardData::Query(void* data, const InputStates state) const
	{
		const int key = *static_cast<int*>(data);

		return (keyArray[key] & state) == state;
	}
}
