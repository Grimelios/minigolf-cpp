#include "InputBind.h"

namespace Minigolf
{
	InputBind::InputBind(const InputTypes type, void* data) : type(type), data(data)
	{
	}

	InputTypes InputBind::Type() const
	{
		return type;
	}

	void InputBind::Type(const InputTypes type)
	{
		this->type = type;
	}

	void* InputBind::Data() const
	{
		return data;
	}

	void InputBind::Data(void* data)
	{
		this->data = data;
	}
}
