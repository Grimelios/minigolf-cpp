#pragma once

namespace Minigolf
{
	class IRotatable
	{
		public:

		virtual ~IRotatable();
		virtual float Rotation() const = 0;
		virtual void Rotation(float rotation) = 0;
	};

	inline IRotatable::~IRotatable() = default;
}
