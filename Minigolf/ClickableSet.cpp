#include "ClickableSet.h"

namespace Minigolf
{
	template <class T>
	ClickableSet<T>::ClickableSet() : hoveredItem(nullptr), focusedItem(nullptr)
	{
	}

	template <class T>
	void ClickableSet<T>::HandleMouse(const MouseData& data)
	{
		const glm::vec2& mousePosition = data.Position();

		if (hoveredItem != nullptr && !hoveredItem->Contains(mousePosition))
		{
			hoveredItem->OnUnhover();
			hoveredItem = nullptr;
		}

		for (const T& item : items)
		{
			if (&item != hoveredItem && item.Contains(mousePosition))
			{
				hoveredItem = &item;
				hoveredItem->OnHover();

				break;
			}
		}

		if (hoveredItem != nullptr && data.Query(GLFW_MOUSE_BUTTON_LEFT, InputStates::PressedThisFrame))
		{
			hoveredItem->OnClick();
		}
	}
}
