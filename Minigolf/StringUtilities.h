#pragma once
#include <string>
#include "SpriteFont.h"
#include <vector>

namespace Minigolf
{
	namespace StringUtilities
	{
		std::vector<std::string> Split(const std::string& s, char delimeter, bool removeEmpty = false);
		std::vector<std::string> WrapLines(const SpriteFont& font, const std::string& s, int targetWidth, int start = 0);
		std::string RemoveExtension(const std::string& s);

		int IndexOf(const std::string& s, char c);
		int IndexOf(const std::string& s, char c, int start);
		int IndexOf(const std::string& s, const std::string& value);
		int IndexOf(const std::string& s, const std::string& value, int start);
		int LastIndexOf(const std::string& s, char c);

		void ReplaceAt(std::string& s, const std::string& value, int index);
	}
}
