#pragma once
#include <glm/detail/type_vec3.hpp>

namespace Minigolf
{
	class VerletPoint
	{
		private:

		glm::vec3 position;
		glm::vec3 previousPosition;

		bool fixed;

		public:

		explicit VerletPoint(glm::vec3 position);

		glm::vec3 Position() const;
		glm::vec3 PreviousPosition() const;

		bool Fixed() const;

		void Position(glm::vec3 position);
	};
}
