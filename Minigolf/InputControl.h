#pragma once
#include "Container2D.h"
#include "IClickable.h"

namespace Minigolf
{
	class InputControl : public Container2D, public IClickable
	{
		public:

		bool Contains(const glm::vec2& mousePosition) override;
	};
}
