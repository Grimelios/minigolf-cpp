#pragma once
#include "DoublyLinkedListNode.h"

namespace Minigolf
{
	template<class T>
	class DoublyLinkedList
	{
	private:

		DoublyLinkedListNode<T>* head = nullptr;
		DoublyLinkedListNode<T>* tail = nullptr;

		int count = 0;

	public:

		~DoublyLinkedList();

		int Count() const;

		DoublyLinkedListNode<T>* Head() const;
		DoublyLinkedListNode<T>* Tail() const;

		void Add(T data);
	};

	template <class T>
	DoublyLinkedList<T>::~DoublyLinkedList()
	{
		DoublyLinkedListNode<T>* node = head;

		while (node != nullptr)
		{
			delete node;
			node = node->Next();
		}
	}

	template <class T>
	int DoublyLinkedList<T>::Count() const
	{
		return count;
	}

	template <class T>
	DoublyLinkedListNode<T>* DoublyLinkedList<T>::Head() const
	{
		return head;
	}

	template <class T>
	DoublyLinkedListNode<T>* DoublyLinkedList<T>::Tail() const
	{
		return tail;
	}

	template <class T>
	void DoublyLinkedList<T>::Add(T data)
	{
		DoublyLinkedListNode<T>* node = new DoublyLinkedListNode<T>(data);

		if (count == 0)
		{
			head = node;
			tail = node;
		}
		else
		{
			node->Previous(tail);
			tail->Next(node);
			tail = node;
		}

		count++;
	}
}
