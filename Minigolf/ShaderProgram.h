#pragma once
#include <string>
#include <glad.h>
#include <vector>
#include <map>

namespace Minigolf
{
	class ShaderProgram
	{
		private:

		static void LoadShader(const std::string& filename, GLenum type, GLuint& shaderId);

		GLuint vShader = -1;
		GLuint fShader = -1;
		GLuint program = -1;

		std::map<std::string, GLint> uniformMap;

		void CreateProgram();
		void GetUniforms();

		public:

		explicit ShaderProgram(const std::string& name);
		ShaderProgram(const std::string& vShader, const std::string& fShader);

		GLuint Program() const;
		GLint Uniforms(const std::string& name);
	};
}
