#pragma once

namespace Minigolf
{
	class IDynamic
	{
		public:

		virtual ~IDynamic();
		virtual void Update(float dt) = 0;
	};

	inline IDynamic::~IDynamic() = default;	
}
