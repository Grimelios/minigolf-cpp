#pragma once
#include "IDynamic.h"
#include <vector>
#include "GraphNode.h"
#include "VerletPoint.h"

namespace Minigolf
{
	class VerletCloth : public IDynamic
	{
		private:

		std::vector<GraphNode<VerletPoint, float>> points;
		std::vector<glm::vec3> newPoints;

		public:

		explicit VerletCloth(std::vector<GraphNode<VerletPoint, float>>&& points);

		void Update(float dt) override;
		static void Draw();
	};
}
