#pragma once
#include "SpriteText.h"
#include "Container2D.h"
#include "MarkdownToken.h"
#include "MarkdownFormatter.h"

namespace Minigolf
{
	class ChatMessage : public Container2D
	{
	private:

		using FontArray = std::array<const SpriteFont*, 4>;

		static MarkdownFormatter formatter;
		static FontArray fontArray;

		SpriteText sender;
		std::vector<glm::ivec2> offsets;
		std::vector<SpriteText> textList;

		int lineCount = 1;

	public:

		// This function is required to avoid read access violations when initializing the font array.
		static void Initialize();

		ChatMessage(const std::string& sender, const std::string& message);

		int LineCount() const;

		void Location(const glm::ivec2& location) override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
