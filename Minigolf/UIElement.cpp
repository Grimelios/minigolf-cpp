#include "UIElement.h"

namespace Minigolf
{
	int UIElement::OffsetX() const
	{
		return offsetX;
	}

	void UIElement::OffsetX(const int offsetX)
	{
		this->offsetX = offsetX;
	}

	int UIElement::OffsetY() const
	{
		return offsetY;
	}

	void UIElement::OffsetY(const int offsetY)
	{
		this->offsetY = offsetY;
	}

	Alignments2D UIElement::Alignment() const
	{
		return alignment;
	}

	void UIElement::Alignment(const Alignments2D alignment)
	{
		this->alignment = alignment;
	}

	void UIElement::HandleMouse(const MouseData& data)
	{
	}
}
