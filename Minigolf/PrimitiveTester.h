#pragma once
#include <glad.h>
#include "ShaderProgram.h"
#include "Texture2D.h"
#include <memory>

namespace Minigolf
{
	class PrimitiveTester
	{
		private:

		ShaderProgram shader;

		GLuint vao = 0;
		GLuint vShader = 0;
		GLuint fShader = 0;
		GLuint program = 0;
		GLuint positionBuffer = 0;
		GLuint texCoordBuffer = 0;
		GLuint colorBuffer = 0;
		GLuint indexBuffer = 0;

		std::unique_ptr<Texture2D> texture;

		public:

		PrimitiveTester();

		void Draw() const;
	};
}
