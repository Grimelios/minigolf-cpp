#include "UIContainer.h"
#include "UIElementData.h"
#include "JsonUtilities.h"
#include "ChatElement.h"
#include "Resolution.h"

namespace Minigolf
{
	UIContainer::TypeMap UIContainer::typeMap
	{
		{ "Chat", ElementTypes::Chat }
	};

	UIContainer::UIContainer(const std::string& filename)
	{
		Json j = JsonUtilities::Deserialize(filename);

		for (Json& element : j)
		{
			const UIElementData data(element);

			std::unique_ptr<UIElement> e = CreateElement(data);
			e->Alignment(data.Alignment());
			e->OffsetX(data.OffsetX());
			e->OffsetY(data.OffsetY());

			elements.push_back(std::move(e));
		}

		PositionElements();
	}

	std::unique_ptr<UIElement> UIContainer::CreateElement(const UIElementData& data)
	{
		switch (typeMap.at(data.Type()))
		{
			case ElementTypes::Chat: return std::make_unique<ChatElement>(ChatElement());
		}

		return nullptr;
	}

	void UIContainer::PositionElements()
	{
		const int windowWidth = Resolution::WindowWidth();
		const int windowHeight = Resolution::WindowHeight();

		for (std::unique_ptr<UIElement>& element : elements)
		{
			const Alignments2D alignment = element->Alignment();

			const bool left = (alignment & Alignments2D::Left) == Alignments2D::Left;
			const bool right = (alignment & Alignments2D::Right) == Alignments2D::Right;
			const bool top = (alignment & Alignments2D::Top) == Alignments2D::Top;
			const bool bottom = (alignment & Alignments2D::Bottom) == Alignments2D::Bottom;

			const int offsetX = element->OffsetX();
			const int offsetY = element->OffsetY();
			const int x = left ? offsetX : (right ? windowWidth - offsetX : windowWidth / 2 + offsetX);
			const int y = top ? offsetY : (bottom ? windowHeight - offsetY : windowHeight / 2 + offsetY);

			element->Location(glm::ivec2(x, y));
		}
	}

	UIElement& UIContainer::GetElement()
	{
		return *elements[0];
	}

	void UIContainer::Update(const float dt)
	{
		for (std::unique_ptr<UIElement>& element : elements)
		{
			if (element->Visible())
			{
				element->Update(dt);
			}
		}
	}

	void UIContainer::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		for (std::unique_ptr<UIElement>& element : elements)
		{
			if (element->Visible())
			{
				element->Draw(sb, pb);
			}
		}
	}
}
