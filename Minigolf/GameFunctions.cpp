#include "GameFunctions.h"
#include <glm/vec2.hpp>

namespace Minigolf
{
	glm::ivec2 GameFunctions::ComputeOrigin(const int width, const int height, const Alignments2D alignment)
	{
		const bool left = (alignment & Alignments2D::Left) == Alignments2D::Left;
		const bool right = (alignment & Alignments2D::Right) == Alignments2D::Right;
		const bool top = (alignment & Alignments2D::Top) == Alignments2D::Top;
		const bool bottom = (alignment & Alignments2D::Bottom) == Alignments2D::Bottom;

		const int x = left ? 0 : (right ? width : width / 2);
		const int y = top ? 0 : (bottom ? height : height / 2);

		return glm::ivec2(x, y);
	}
}
