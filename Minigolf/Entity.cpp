#include "Entity.h"

namespace Minigolf
{
	Entity::Entity() : position(0), orientation(glm::vec3(0))
	{
	}

	glm::vec3 Entity::Position() const
	{
		return position;
	}

	glm::quat Entity::Orientation() const
	{
		return orientation;
	}

	void Entity::Position(const glm::vec3 position)
	{
		this->position = position;
	}

	void Entity::Orientation(const glm::quat& orientation)
	{
		this->orientation = glm::quat(orientation);
	}

	void Entity::Update(float dt)
	{
	}
}
