#include "ContentCache.h"
#include "MapUtilities.h"

namespace Minigolf
{
	FontLoader ContentCache::fontLoader;
	ModelLoader ContentCache::modelLoader;
	TextureLoader ContentCache::textureLoader;

	ContentCache::FontCache ContentCache::fontCache;
	ContentCache::ModelCache ContentCache::modelCache;
	ContentCache::TextureCache ContentCache::textureCache;

	Model& ContentCache::GetModel(const std::string& filename)
	{
		return GetGeneric<Model>(filename, modelCache, modelLoader);
	}

	const SpriteFont& ContentCache::GetFont(const std::string& name, const bool useKerning)
	{
		const SpriteFont* value;

		if (!MapUtilities::TryGetValue(fontCache, name, value))
		{
			const SpriteFont font = fontLoader.Load(name, useKerning);
			MapUtilities::Add(fontCache, name, font);
			value = &fontCache.find(name)->second;
		}

		return *value;
	}

	Texture2D& ContentCache::GetTexture(const std::string& filename, const std::string& folder)
	{
		return GetGeneric<Texture2D>(folder + filename, textureCache, textureLoader);
	}
}
