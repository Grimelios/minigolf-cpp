#pragma once
#include <vector>
#include "GraphConnection.h"

namespace Minigolf
{
	template<class T, class E>
	class GraphNode
	{
		private:

		T data;
		std::vector<GraphConnection<T, E>> neighbors;

		public:

		explicit GraphNode(T data);

		T& Data();
		const std::vector<GraphConnection<T, E>>& Neighbors() const;

		void AddNeighbor(GraphNode<T, E> node, E edge);
	};

	template <class T, class E>
	GraphNode<T, E>::GraphNode(T data)
	{
		this->data = data;
	}

	template <class T, class E>
	T& GraphNode<T, E>::Data()
	{
		return data;
	}

	template <class T, class E>
	const std::vector<GraphConnection<T, E>>& GraphNode<T, E>::Neighbors() const
	{
		return neighbors;
	}

	template <class T, class E>
	void GraphNode<T, E>::AddNeighbor(GraphNode<T, E> node, E edge)
	{
		neighbors.push_back(GraphConnection<T, E>(node.Data(), edge));
	}
}
