#pragma once
#include <map>

namespace Minigolf
{
	namespace MapUtilities
	{
		template<class K, class V>
		bool TryGetValue(std::map<K, V>& map, const K& key, V*& value)
		{
			auto iterator = map.find(key);

			if (iterator == map.end())
			{
				return false;
			}

			value = &iterator->second;

			return true;
		}

		template<class K, class V>
		bool TryGetValue(const std::map<K, V>& map, const K& key, const V*& value)
		{
			auto iterator = map.find(key);

			if (iterator == map.end())
			{
				return false;
			}

			value = &iterator->second;

			return true;
		}

		template<class K, class V>
		void Add(std::map<K, V>& map, const K key, const V value)
		{
			// See https://stackoverflow.com/questions/4286670/preferred-idiomatic-way-to-insert-into-a-map.
			map.insert(std::map<K, V>::value_type(key, value));
		}

		template<class K, class V>
		V& Get(std::map<K, V>& map, const K& key)
		{
			return map.find(key)->second;
		}
	}
}
