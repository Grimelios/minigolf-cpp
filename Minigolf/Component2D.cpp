#include "Component2D.h"

namespace Minigolf
{
	Component2D::Component2D(const Alignments2D alignment) : alignment(alignment)
	{
	}

	glm::vec2 Component2D::Position() const
	{
		return position;
	}

	glm::vec2 Component2D::Scale() const
	{
		return scale;
	}

	glm::ivec2 Component2D::Origin() const
	{
		return origin;
	}

	float Component2D::Rotation() const
	{
		return rotation;
	}

	glm::vec4 Component2D::Color() const
	{
		return color;
	}

	void Component2D::Position(const glm::vec2 position)
	{
		this->position = position;
	}

	void Component2D::Scale(const glm::vec2 scale)
	{
		this->scale = scale;
	}

	void Component2D::Origin(const glm::ivec2 origin)
	{
		this->origin = origin;
	}

	void Component2D::Rotation(const float rotation)
	{
		this->rotation = rotation;
	}

	void Component2D::Color(const glm::vec4& color)
	{
		this->color = color;
	}
}
