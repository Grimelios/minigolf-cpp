#include "FontLoader.h"
#include "ContentCache.h"
#include "FileUtilities.h"
#include "Paths.h"
#include "StringUtilities.h"

namespace Minigolf
{
	const SpriteFont FontLoader::Load(const std::string& name, const bool useKerning) const
	{
		const Texture2D& texture = ContentCache::GetTexture(name + "_0.png", Paths::Fonts);

		const int tWidth = texture.Width();
		const int tHeight = texture.Height();

		const std::vector<std::string> lines = FileUtilities::ReadAllLines(Paths::Fonts + name + ".fnt");
		const std::string& line0 = lines[0];

		// The third token on the first line is "size=[value]", but spaces in the font name must be accounted for.
		const int index1 = StringUtilities::IndexOf(line0, "size") + 5;
		const int index2 = StringUtilities::IndexOf(line0, ' ', index1);
		const int size = std::stoi(line0.substr(index1, index2 - index1));

		// Actual character data begins at line index four.
		unsigned int lineIndex = 4;

		SpriteFont::CharacterArray dataArray;

		while (lineIndex < lines.size())
		{
			const std::string& line = lines[lineIndex];

			// Lines starting with 'k' are kerning lines.
			if (line[0] == 'k')
			{
				lineIndex++;

				break;
			}

			std::vector<std::string> tokens = StringUtilities::Split(line, ' ', true);

			const int index = ParseValue(tokens[1]);
			const int x = ParseValue(tokens[2]);
			const int y = ParseValue(tokens[3]);
			const int width = ParseValue(tokens[4]);
			const int height = ParseValue(tokens[5]);
			const int offsetX = ParseValue(tokens[6]);
			const int offsetY = ParseValue(tokens[7]);
			const int advance = ParseValue(tokens[8]);

			dataArray[index] = CharacterData(x, y, width, height, advance, tWidth, tHeight,	glm::ivec2(offsetX, offsetY));
			lineIndex++;
		}

		if (useKerning)
		{
		}

		return SpriteFont(texture, size, dataArray);
	}

	int FontLoader::ParseValue(const std::string& s) const
	{
		const int index = StringUtilities::IndexOf(s, '=') + 1;

		return std::stoi(s.substr(index));
	}

	void FontLoader::ParseKerning()
	{
	}
}
