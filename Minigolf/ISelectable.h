#pragma once

namespace Minigolf
{
	class ISelectable
	{
		public:

		virtual ~ISelectable();
		virtual void OnSelect() = 0;
		virtual void OnDeselect() = 0;
		virtual void OnSubmit() = 0;
	};

	inline ISelectable::~ISelectable() = default;
}
