#include "HoleData.h"
#include <utility>

namespace Minigolf
{
	HoleData::HoleData(const int number, const int par, std::string name, std::string designer) :
		number(number), par(par), name(std::move(name)), designer(std::move(designer))
	{
	}

	int HoleData::Number() const
	{
		return number;
	}

	int HoleData::Par() const
	{
		return par;
	}

	const std::string& HoleData::Name() const
	{
		return name;
	}

	const std::string& HoleData::Designer() const
	{
		return designer;
	}
}
