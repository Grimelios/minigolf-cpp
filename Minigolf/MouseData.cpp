#include "MouseData.h"

namespace Minigolf
{
	MouseData::MouseData(const glm::vec2 position, const glm::vec2 previousPosition,
		const std::array<InputStates, GLFW_MOUSE_BUTTON_LAST>& buttonArray) :
		buttonArray(buttonArray), position(position), previousPosition(previousPosition)
	{
	}

	const glm::vec2& MouseData::Position() const
	{
		return position;
	}

	const glm::vec2& MouseData::PreviousPosition() const
	{
		return previousPosition;
	}

	bool MouseData::Query(void* data, const InputStates state) const
	{
		const int button = *static_cast<int*>(data);

		return (buttonArray[button] & state) == state;
	}
}
