#include "Resolution.h"

namespace Minigolf
{
	int Resolution::Width()
	{
		return width;
	}

	int Resolution::Height()
	{
		return height;
	}

	int Resolution::WindowWidth()
	{
		return windowWidth;
	}

	int Resolution::WindowHeight()
	{
		return windowHeight;
	}
}
