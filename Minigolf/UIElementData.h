#pragma once
#include "Enumerations.h"
#include <nlohmann/json.hpp>
#include "CommonAliases.h"

namespace Minigolf
{
	class UIElementData
	{
	private:

		Alignments2D alignment = Alignments2D::Unassigned;

		int offsetX = 0;
		int offsetY = 0;

		std::string type;

	public:

		explicit UIElementData(const Json& j);

		Alignments2D Alignment() const;

		int OffsetX() const;
		int OffsetY() const;

		const std::string& Type() const;
	};
}
