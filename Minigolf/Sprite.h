#pragma once
#include "Component2D.h"
#include "Texture2D.h"
#include "Enumerations.h"
#include <string>

namespace Minigolf
{
	class Sprite : public Component2D
	{
		private:

		const Texture2D& texture;
		std::optional<Rectangle> sourceRect;

		public:

		explicit Sprite(const std::string& texture, Alignments2D alignment = Alignments2D::Center);
		explicit Sprite(const Texture2D& texture, Alignments2D alignment = Alignments2D::Center);

		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
