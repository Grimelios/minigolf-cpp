#pragma once
#include "UIElement.h"
#include "SpriteText.h"

namespace Minigolf
{
	class StrokeDisplay : public UIElement
	{
		private:

		int strokeCount = 0;

		SpriteText spriteText;

		public:

		StrokeDisplay();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
