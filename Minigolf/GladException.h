#pragma once
#include <exception>
#include <string>

namespace Minigolf
{
	class GladException : public std::exception
	{
		public:

		explicit GladException(const std::string& message);
	};
}
