#include "VerletPoint.h"

namespace Minigolf
{
	VerletPoint::VerletPoint(glm::vec3 position) : position(position), previousPosition(position)
	{
	}

	glm::vec3 VerletPoint::Position() const
	{
		return position;
	}

	glm::vec3 VerletPoint::PreviousPosition() const
	{
		return previousPosition;
	}

	bool VerletPoint::Fixed() const
	{
		return fixed;
	}

	void VerletPoint::Position(const glm::vec3 position)
	{
		previousPosition = position;
		this->position = position;
	}
}
