#pragma once
#include <glm/detail/type_vec2.hpp>

namespace Minigolf
{
	class IClickable
	{
		public:

		virtual ~IClickable();
		virtual void OnHover() = 0;
		virtual void OnUnhover() = 0;
		virtual void OnClick() = 0;
		virtual bool Contains(const glm::vec2& mousePosition) = 0;
	};

	inline IClickable::~IClickable() = default;
}
