#pragma once
#include <vector>
#include "MouseData.h"

namespace Minigolf
{
	template<class T>
	class ClickableSet
	{
		private:

		std::vector<T> items;
		T* hoveredItem;

		public:

		ClickableSet();

		void HandleMouse(const MouseData& data);
	};
}
