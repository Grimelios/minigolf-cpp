#pragma once
#include "GameLoop.h"

namespace Minigolf
{
	class ViewerLoop : public GameLoop
	{
		private:

		Model& model;
		ShaderProgram shader;
		glm::mat4 projection;

		public:

		ViewerLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch2D& pb);

		void Initialize() override;
		void Update(float dt) override;
		void Draw() override;
	};
}
