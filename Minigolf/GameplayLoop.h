#pragma once
#include "GameLoop.h"
#include "HeadsUpDisplay.h"

namespace Minigolf
{
	class GameplayLoop : public GameLoop
	{
	private:

		HeadsUpDisplay hud;

	public:

		GameplayLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch2D& pb);

		void Initialize() override;
		void Update(float dt) override;
		void Draw() override;
	};
}
