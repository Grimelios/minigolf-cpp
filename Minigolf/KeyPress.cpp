#include "KeyPress.h"

namespace Minigolf
{
	KeyPress::KeyPress(const int key, const int mods) : key(key), mods(mods)
	{
	}

	int KeyPress::Key() const
	{
		return key;
	}

	int KeyPress::Mods() const
	{
		return mods;
	}
}
