#pragma once
#include <string>

namespace Minigolf
{
	class HoleData
	{
		private:

		int number;
		int par;

		std::string name;
		std::string designer;

		public:

		HoleData(int number, int par, std::string name, std::string designer);

		int Number() const;
		int Par() const;

		const std::string& Name() const;
		const std::string& Designer() const;
	};
}
