#include "InputControl.h"

namespace Minigolf
{
	bool InputControl::Contains(const glm::vec2& mousePosition)
	{
		return bounds.Contains(mousePosition);
	}
}
