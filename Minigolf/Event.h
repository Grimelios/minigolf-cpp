#pragma once
#include <vector>
#include <functional>

namespace Minigolf
{
	template<class ... Types>
	class Event
	{
		using ListenerFunction = std::function<void(Types...)>;

		private:

		std::vector<ListenerFunction> listeners;

		public:

		void Add(const ListenerFunction& function);
		void Remove(const ListenerFunction& function);
		void Trigger(Types... types);
	};

	template<class ... Types>
	void Event<Types...>::Add(const ListenerFunction& function)
	{
		listeners.emplace_back(function);
	}

	template <class ... Types>
	void Event<Types...>::Remove(const ListenerFunction& function)
	{
	}

	template <class ... Types>
	void Event<Types...>::Trigger(Types... types)
	{
		for (ListenerFunction& l : listeners)
		{
			l(types...);
		}
	}
}
