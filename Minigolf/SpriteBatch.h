#pragma once

#include "Texture2D.h"
#include "ShaderProgram.h"
#include <array>
#include "SpriteFont.h"
#include "Rectangle.h"
#include <glm/detail/type_vec2.hpp>
#include <glm/detail/type_vec4.hpp>
#include "BatchBase.h"
#include "PrimitiveBatch2D.h"
#include "SpriteGroup.h"

namespace Minigolf
{
	class PrimitiveBatch2D;
	class SpriteBatch : public BatchBase
	{
	private:

		using InnerMap = std::map<const Texture2D*, SpriteGroup>;
		using SpriteMap = std::map<const ShaderProgram*, InnerMap>;

		const ShaderProgram spriteShader;
		PrimitiveBatch2D* pb = nullptr;
		const Texture2D* activeTexture = nullptr;
		SpriteGroup* activeGroup = nullptr;
		InnerMap* activeMap = nullptr;

		std::array<glm::vec2, 4> quadTexCoords;
		std::array<int, 6> quadIndices;

		GLuint vao = 0;
		GLuint program = 0;
		GLuint indexBuffer = 0;
		GLuint positionBuffer = 0;
		GLuint texCoordBuffer = 0;
		GLuint quadCoordBuffer = 0;
		GLuint colorBuffer = 0;

		SpriteMap spriteMap;

		void Flush(SpriteGroup& group);
		void VerifyTexture(const Texture2D& texture);

	public:

		SpriteBatch();

		void SetOther(PrimitiveBatch2D* pb);
		void Apply(const ShaderProgram& shader);
		void Draw(const Texture2D& texture, const std::optional<Rectangle>& sourceRect, glm::vec2 position, glm::ivec2 origin,
			float rotation, const glm::vec4& color, const glm::vec2& scale);

		void DrawString(const SpriteFont& font, const std::string& value, const glm::vec2& position, const glm::vec4& color);
		void DrawString(const SpriteFont& font, const std::string& value, const glm::vec2& position, const glm::ivec2& origin,
			float rotation, const glm::vec4& color, const glm::vec2& scale);

		void Flush() override;
	};
}
