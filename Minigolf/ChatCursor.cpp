#include "ChatCursor.h"
#include "MathHelper.h"
#include "Properties.h"

namespace Minigolf
{
	ChatCursor::ChatCursor()
	{
		Properties::PropertyMap& map = Properties::Load("UI.properties");

		halfLength = std::stoi(map.at("Chat.Cursor.Length")) / 2;
		correction = std::stoi(map.at("Chat.Cursor.Correction"));
		blinkTime = std::stoi(map.at("Chat.Cursor.Blink.Time"));
		stallTime = std::stoi(map.at("Chat.Cursor.Stall.Time"));
		fadeFraction = std::stof(map.at("Chat.Cursor.Fade.Fraction"));

		tick = [this](const float progress)
		{
			float percentage = std::max(0.0f, 1 - (1 - progress) / fadeFraction);

			if (fadingOut)
			{
				percentage = 1 - percentage;
			}

			color.a = MathHelper::Lerp(0, 1, percentage);
		};

		blinkTimer = std::make_unique<Timer>(blinkTime, Timer::RFunction([this](const float time) -> bool
		{
			fadingOut = !fadingOut;

			return true;
		}));

		blinkTimer->Tick(&tick);

		stallTimer = std::make_unique<Timer>(stallTime, Timer::NFunction([this](const float time)
		{
			blinkTimer->Paused(false);
			stallTimer->Reset();
			stallTimer->Paused(true);
		}));

		stallTimer->Paused(true);
	}

	void ChatCursor::Location(const glm::ivec2& location)
	{
		// Offsetting the cursor by a small amount is better visually since there's a tiny bit of padding.
		const int corrected = location.x + correction;

		// The given location is vertically centered within the line. This makes it easier to modify the cursor length.
		start = glm::ivec2(corrected, location.y - halfLength);
		end = glm::ivec2(corrected, location.y + halfLength);
	}

	void ChatCursor::Color(const glm::vec4& color)
	{
		this->color = color;
	}

	void ChatCursor::Stall()
	{
		if (stallTimer->Paused())
		{
			color.a = 1;
			fadingOut = true;
			blinkTimer->Reset();
			blinkTimer->Paused(true);
			stallTimer->Paused(false);
		}
		else
		{
			// Using this approach, the stall timer is constantly reset as characters are typed.
			stallTimer->Reset();
		}
	}

	void ChatCursor::Update(const float dt)
	{
		blinkTimer->Update(dt);
		stallTimer->Update(dt);
	}

	void ChatCursor::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		// Alpha can be zero while blinking.
		if (color.a == 0)
		{
			return;
		}

		pb.DrawLine(start, end, color);
	}
}
