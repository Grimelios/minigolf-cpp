#pragma once

namespace Minigolf
{
	class KeyPress
	{
		private:

		int key;
		int mods;

		public:

		KeyPress(int key, int mods);

		int Key() const;
		int Mods() const;
	};
}
