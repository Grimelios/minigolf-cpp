#pragma once
#include "IDynamic.h"
#include "SpriteBatch.h"
#include "Camera.h"
#include "PrimitiveBatch2D.h"

namespace Minigolf
{
	class GameLoop : public IDynamic
	{
		protected:

		Camera& camera;
		SpriteBatch& sb;
		PrimitiveBatch2D& pb;

		public:

		GameLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch2D& pb);

		virtual ~GameLoop() = 0;
		virtual void Initialize() = 0;
		virtual void Draw() = 0;
	};

	inline GameLoop::~GameLoop() = default;
}
