#pragma once

namespace Minigolf
{
	class Resolution
	{
	private:

		// These values should be made non-const later to allow dynamic window changes.
		static const int width = 800;
		static const int height = 600;
		static const int windowWidth = 800;
		static const int windowHeight = 600;

	public:

		static int Width();
		static int Height();
		static int WindowWidth();
		static int WindowHeight();
	};
}
