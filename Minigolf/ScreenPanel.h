#pragma once
#include "UIElement.h"

namespace Minigolf
{
	class ScreenPanel : public UIElement
	{
		public:

		virtual ~ScreenPanel() = 0;
	};

	inline ScreenPanel::~ScreenPanel() = default;
}
