#pragma once
#include <glm/detail/type_vec2.hpp>

namespace Minigolf
{
	class IPositionable2D
	{
		public:

		virtual ~IPositionable2D();
		virtual glm::vec2 Position() const = 0;
		virtual void Position(glm::vec2 position) = 0;
	};

	inline IPositionable2D::~IPositionable2D() = default;
}
