#pragma once
#include <string>
#include "MarkdownTag.h"

namespace Minigolf
{
	class MarkdownToken
	{
	private:

		std::string value;
		MarkdownTypes type;

	public:

		MarkdownToken(std::string value, MarkdownTypes type);

		const std::string& Value() const;
		MarkdownTypes Type() const;
	};
}
