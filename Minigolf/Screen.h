#pragma once
#include "IDynamic.h"
#include "IRenderable2D.h"
#include "ScreenPanel.h"

namespace Minigolf
{
	class Screen : public IDynamic, public IRenderable2D
	{
		private:

		static std::unique_ptr<ScreenPanel> CreatePanel(const std::string& type);

		std::vector<std::unique_ptr<ScreenPanel>> panels;

		protected:

		explicit Screen(const std::string& filename);

		public:

		virtual ~Screen() = 0;

		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};

	inline Screen::~Screen() = default;
}
