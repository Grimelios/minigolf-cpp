#include "Timer.h"
#include <utility>

namespace Minigolf
{
	Timer::Timer(const int duration, const NFunction& function, const float elapsed) :
		Timer(static_cast<float>(duration), function, elapsed)
	{
	}

	Timer::Timer(const float duration, NFunction function, const float elapsed) :
		// Dividing by 1000 means that delta time doesn't need to be re-multiplied in the update function.
		elapsed(elapsed / 1000),
		duration(duration / 1000),
		repeating(false),
		nFunction(std::move(function))
	{
	}

	Timer::Timer(const int duration, const RFunction& function, const float elapsed) :
		Timer(static_cast<float>(duration), function, elapsed)
	{
	}

	Timer::Timer(const float duration, RFunction function, const float elapsed) :
		elapsed(elapsed / 1000),
		duration(duration / 1000),
		repeating(true),
		rFunction(std::move(function))
	{
	}

	bool Timer::Paused() const
	{
		return paused;
	}

	void Timer::Paused(const bool paused)
	{
		this->paused = paused;
	}

	// Passing tick function pointers into the constructor is difficult since lambdas are temporary. As a result, classes
	// that use a tick function should store their own std::function locally, then pass the address here.
	void Timer::Tick(TFunction* tFunction)
	{
		this->tFunction = tFunction;
	}

	void Timer::Reset()
	{
		elapsed = 0;
	}

	void Timer::SetTimerCollection(TimerCollection* collection)
	{
		this->collection = collection;
	}

	void Timer::Update(const float dt)
	{
		if (paused)
		{
			return;
		}

		elapsed += dt;

		if (repeating)
		{
			// Trigger functions can pause the timer. Also note that using a loop is required for durations less than half a
			// frame (such that the timer could trigger multiple times per frame).
			while (elapsed >= duration && !paused)
			{
				elapsed -= duration;

				if (!rFunction(elapsed))
				{
					if (collection != nullptr)
					{
						collection->Remove(*this);
					}

					return;
				}
			}
		}
		else if (elapsed >= duration)
		{
			// Tick functions are intentionally not called for a trigger. As such, trigger functions should handle any final
			// tick behavior required.
			nFunction(elapsed - duration);

			if (collection != nullptr)
			{
				collection->Remove(*this);
			}

			return;
		}

		if (tFunction != nullptr)
		{
			(*tFunction)(elapsed / duration);
		}
	}

	bool Timer::operator==(const Timer& other) const
	{
		return this == &other;
	}
}
