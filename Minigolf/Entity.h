#pragma once
#include "IPositionable3D.h"
#include "IDynamic.h"
#include "Model.h"
#include "IOrientable.h"

namespace Minigolf
{
	class Entity : public IPositionable3D, public IOrientable, public IDynamic
	{
		protected:

		glm::vec3 position;
		glm::quat orientation;

		public:

		Entity();
		virtual ~Entity() = 0;

		glm::vec3 Position() const override;
		glm::quat Orientation() const override;

		void Position(glm::vec3 position) override;
		void Orientation(const glm::quat& orientation) override;
		void Update(float dt) override;
	};

	inline Entity::~Entity() = default;
}
