#pragma once
#include "Container2D.h"
#include "ISelectable.h"
#include "IClickable.h"
#include "Menu.h"
#include "SpriteText.h"

namespace Minigolf
{
	class Menu;
	class MenuItem : public Container2D, public ISelectable, public IClickable
	{
		private:

		SpriteText spriteText;
		const Menu* parent = nullptr;

		int index = -1;

		public:

		MenuItem(const std::string& value, int index, const Menu* parent);

		void OnSelect() override;
		void OnDeselect() override;
		void OnSubmit() override;
		void OnHover() override;
		void OnUnhover() override;
		void OnClick() override;
		bool Contains(const glm::vec2& mousePosition) override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
