#pragma once

namespace Minigolf
{
	class IScalable2D
	{
		public:

		virtual ~IScalable2D();
		virtual glm::vec2 Scale() const = 0;
		virtual void Scale(glm::vec2 scale) = 0;
	};

	inline IScalable2D::~IScalable2D() = default;
}
