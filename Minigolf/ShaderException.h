#pragma once
#include <exception>
#include <string>

namespace Minigolf
{
	enum class ShaderExceptionTypes
	{
		Compile,
		Link
	};

	class ShaderException : public std::exception
	{
		public:

		explicit ShaderException(ShaderExceptionTypes type, const std::string& message);
	};
}
