#pragma once
#include "UIElement.h"
#include "Textbox.h"
#include "TextboxChatRenderer.h"
#include "ChatWindow.h"

namespace Minigolf
{
	class ChatElement : public UIElement
	{
	private:

		TextboxChatRenderer textboxRenderer;
		Textbox textbox;
		ChatWindow chatWindow;

	public:

		ChatElement();

		void Location(const glm::ivec2& location) override;
		void HandleKeyboard(const KeyboardData& data);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
