#include "PrimitiveTester.h"
#include "GLUtilities.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "FileUtilities.h"
#include "TextureLoader.h"
#include "VectorUtilities.h"
#include <array>

namespace Minigolf
{
	PrimitiveTester::PrimitiveTester() : shader("Primitive2D")
	{
		/*
		std::string vSource = "#version 330 core \n layout (location = 0) in vec2 vPosition; layout (location = 1) in vec2 vTexCoords; layout (location = 2) in vec4 vColor; out vec2 fTexCoords; out vec4 fColor; void main() { vec4 position = vec4(vPosition, 0, 1); position.y *= -1; gl_Position = position; fTexCoords = vTexCoords; fColor = vColor; }";
		std::string fSource = "#version 330 core \n in vec2 fTexCoords; in vec4 fColor; uniform sampler2D sampler; void main() { gl_FragColor = fColor * texture2D(sampler, fTexCoords); }";

		GLchar const* vPointer = vSource.c_str();
		GLchar const* fPointer = fSource.c_str();

		vShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vShader, 1, &vPointer, nullptr);
		glCompileShader(vShader);

		fShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fShader, 1, &fPointer, nullptr);
		glCompileShader(fShader);

		program = glCreateProgram();
		glAttachShader(program, vShader);
		glAttachShader(program, fShader);
		glLinkProgram(program);
		glDeleteShader(vShader);
		glDeleteShader(fShader);
		*/

		program = shader.Program();

		glGenBuffers(1, &positionBuffer);
		glGenBuffers(1, &texCoordBuffer);
		glGenBuffers(1, &colorBuffer);
		glGenBuffers(1, &indexBuffer);
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8, nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordBuffer);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8, nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 16, nullptr);
		
		for (GLuint i = 0; i < 3; i++)
		{
			glEnableVertexAttribArray(i);
		}

		glBindVertexArray(0);

		TextureLoader textureLoader;
		texture = textureLoader.Load("Content/Textures/Robbaz.png");
	}

	void PrimitiveTester::Draw() const
	{
		const glm::vec2 dimensions = glm::vec2(texture->Width(), texture->Height());
		const glm::vec2 origin = glm::vec2(dimensions.x / 2, dimensions.y / 2);

		std::array<glm::vec2, 4> vertexArray = std::array<glm::vec2, 4>(
		{
			glm::vec2(0.0f),
			glm::vec2(dimensions.x, 0.0f),
			glm::vec2(dimensions),
			glm::vec2(0.0f, dimensions.y)
		});

		for (glm::vec2& v : vertexArray)
		{
			v -= origin;
			v.x /= 400;
			v.y /= 300;
		}

		std::vector<glm::vec2> texCoords;
		texCoords.emplace_back(0.0f);
		texCoords.emplace_back(1.0f, 0.0f);
		texCoords.emplace_back(1.0f, 1.0f);
		texCoords.emplace_back(0.0f, 1.0f);

		std::vector<glm::vec2> vertices;
		std::vector<glm::vec4> colors;

		VectorUtilities::PushArray(vertices, vertexArray);
		VectorUtilities::Repeat(colors, glm::vec4(1.0f), 4);

		std::vector<int> indices;
		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(2);
		indices.push_back(0);
		indices.push_back(2);
		indices.push_back(3);

		glUseProgram(program);
		glBindVertexArray(vao);

		GLUtilities::BufferData(positionBuffer, vertices, sizeof(glm::vec2));
		GLUtilities::BufferData(texCoordBuffer, texCoords, sizeof(glm::vec2));
		GLUtilities::BufferData(colorBuffer, colors, sizeof(glm::vec4));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture->TextureId());
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr);
	}
}
