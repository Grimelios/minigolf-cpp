#pragma once
#include <glm/gtc/constants.hpp>

namespace Minigolf::MathHelper
{
	const float Pi = glm::pi<float>();
	const float PiOverTwo = glm::half_pi<float>();
	const float PiOverFour = glm::quarter_pi<float>();
	const float TwoPi = glm::two_pi<float>();

	float Lerp(float f1, float f2, float amount);
}
