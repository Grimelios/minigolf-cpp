#pragma once
#include <map>

namespace Minigolf
{
	class Properties
	{
	public:

		using PropertyMap = std::map<std::string, std::string>;
		using PropertyCache = std::map<std::string, PropertyMap>;

	private:

		static PropertyCache cache;

	public:

		static PropertyMap& Load(const std::string& filename);
	};
}
