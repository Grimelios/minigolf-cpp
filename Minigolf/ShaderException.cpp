#include "ShaderException.h"

namespace Minigolf
{
	ShaderException::ShaderException(ShaderExceptionTypes type, const std::string& message) :
		exception(message.c_str())
	{
	}
}
