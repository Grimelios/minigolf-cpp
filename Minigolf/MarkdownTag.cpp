#include "MarkdownTag.h"

namespace Minigolf
{
	MarkdownTag::MarkdownTag(const int start, const MarkdownTypes type) :
		MarkdownTag(start, -1, type)
	{
	}

	MarkdownTag::MarkdownTag(const int start, const int end, const MarkdownTypes type) :
		start(start), end(end), type(type)
	{
	}

	int MarkdownTag::Start() const
	{
		return start;
	}

	int& MarkdownTag::End()
	{
		return end;
	}

	MarkdownTypes MarkdownTag::Type() const
	{
		return type;
	}
}
