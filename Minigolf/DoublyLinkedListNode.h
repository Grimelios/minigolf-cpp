#pragma once

namespace Minigolf
{
	template<class T>
	class DoublyLinkedListNode
	{
	private:

		T data;

		DoublyLinkedListNode<T>* next = nullptr;
		DoublyLinkedListNode<T>* previous = nullptr;

	public:

		explicit DoublyLinkedListNode(T data);

		T& Data();

		DoublyLinkedListNode<T>* Next() const;
		DoublyLinkedListNode<T>* Previous() const;

		void Next(DoublyLinkedListNode<T>* next);
		void Previous(DoublyLinkedListNode<T>* previous);
	};

	template <class T>
	DoublyLinkedListNode<T>::DoublyLinkedListNode(T data) : data(std::move(data))
	{
	}

	template <class T>
	T& DoublyLinkedListNode<T>::Data()
	{
		return data;
	}

	template <class T>
	DoublyLinkedListNode<T>* DoublyLinkedListNode<T>::Next() const
	{
		return next;
	}

	template <class T>
	DoublyLinkedListNode<T>* DoublyLinkedListNode<T>::Previous() const
	{
		return previous;
	}

	template <class T>
	void DoublyLinkedListNode<T>::Next(DoublyLinkedListNode<T>* next)
	{
		this->next = next;
	}

	template <class T>
	void DoublyLinkedListNode<T>::Previous(DoublyLinkedListNode<T>* previous)
	{
		this->previous = previous;
	}
}
