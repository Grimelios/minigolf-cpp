#include "CharacterData.h"

namespace Minigolf
{
	CharacterData::CharacterData(const int x, const int y, const int width, const int height, const int advance,
		const int tWidth, const int tHeight, const glm::ivec2 offset) :
		width(width), height(height), advance(advance), offset(offset)
	{
		const float fX = static_cast<float>(x) / tWidth;
		const float fY = static_cast<float>(y) / tHeight;
		const float fWidth = static_cast<float>(width) / tWidth;
		const float fHeight = static_cast<float>(height) / tHeight;

		texCoords =
		{
			glm::vec2(fX, fY),
			glm::vec2(fX + fWidth, fY),
			glm::vec2(fX + fWidth, fY + fHeight),
			glm::vec2(fX, fY + fHeight)
		};
	}

	int CharacterData::Width() const
	{
		return width;
	}

	int CharacterData::Height() const
	{
		return height;
	}

	int CharacterData::Advance() const
	{
		return advance;
	}

	const std::array<glm::vec2, 4>& CharacterData::TexCoords() const
	{
		return texCoords;
	}

	glm::ivec2 CharacterData::Offset() const
	{
		return offset;
	}
}
