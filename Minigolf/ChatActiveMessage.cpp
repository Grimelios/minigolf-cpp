#include "ChatActiveMessage.h"
#include "ContentCache.h"

namespace Minigolf
{
	ChatActiveMessage::ChatActiveMessage() :
		font(ContentCache::GetFont("Chat"))
	{
		// Adding an "empty" entry to beginning of each list simplifies the rest of the class.
		lines.emplace_back("");
		widths.emplace_back(0);
		locations.emplace_back(0);
	}

	glm::ivec2 ChatActiveMessage::ComputeCursorPosition(int cursor)
	{
		if (lines[0].empty())
		{
			return locations[0] + glm::ivec2(0, font.Size() / 2);
		}

		int lineIndex = 0;
		int length = 0;

		// If the cursor is equal to the current line length, the cursor is at the end of that line.
		while (cursor > (length = static_cast<int>(lines[lineIndex].size())))
		{
			lineIndex++;
			cursor -= length;
		}

		const std::string& line = lines[lineIndex];

		int x = 0;

		// Fonts measure strings by summing advance values plus the width of the final character. For the cursor, though,
		// advance must be used all the way through.
		for (int i = 0; i < cursor; i++)
		{
			x += font.Data(line[i]).Advance();
		}

		// Measuring the string takes the font size into account already.
		return locations[lineIndex] + glm::ivec2(x, font.Size() / 2);
	}

	void ChatActiveMessage::Clear()
	{
		// Even if the chat box is completely cleared, it's easier to use empty values rather than full-clearing each vector.
		lines.clear();
		lines.emplace_back("");

		widths.clear();
		widths.emplace_back(0);

		locations.clear();
		locations.emplace_back(location + glm::ivec2(nameOffset, 0));

		lastSpace = -1;
		lineIndex = 0;
	}

	void ChatActiveMessage::Value(const std::string& value)
	{
		lines[0] = value;
	}

	void ChatActiveMessage::Modify(const int cursor, const int removalCount, const std::string& s)
	{
		std::string& firstLine = lines[0];

		if (removalCount > 0)
		{
			firstLine.erase(cursor, removalCount);
		}

		if (!s.empty())
		{
			firstLine.insert(cursor, s);
		}
	}

	void ChatActiveMessage::NameOffset(const int offset)
	{
		nameOffset = offset;
		locations[0] = location + glm::ivec2(offset, 0);
	}

	void ChatActiveMessage::TargetWidth(const int targetWidth)
	{
		this->targetWidth = targetWidth;
	}

	void ChatActiveMessage::Location(const glm::ivec2& location)
	{
		locations[0] = glm::ivec2(location.x + nameOffset, location.y);

		SimpleComponent2D::Location(location);
	}

	void ChatActiveMessage::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		for (int i = 0; i < static_cast<int>(lines.size()); i++)
		{
			sb.DrawString(font, lines[i], locations[i], color);
		}
	}
}
