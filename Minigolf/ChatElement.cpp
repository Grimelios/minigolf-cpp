#include "ChatElement.h"
#include "TextboxChatRenderer.h"
#include "ContentCache.h"

namespace Minigolf
{
	ChatElement::ChatElement() :
		textbox(textboxRenderer, [this](const std::string& message)
		{
			chatWindow.Add(message);
		}),

		chatWindow("Grimelios")
	{
		textboxRenderer.PlayerName("Grimelios");
		textboxRenderer.TargetWidth(400);
	}

	void ChatElement::Location(const glm::ivec2& location)
	{
		// Since the chat window lives in the bottom-left corner of the screen, the location corresponds to that same corner.
		textbox.Location(location - textbox.Height());
		chatWindow.Location(textbox.Location() - glm::ivec2(0, chatWindow.Height()));

		Container2D::Location(location);
	}

	void ChatElement::HandleKeyboard(const KeyboardData& data)
	{
		textbox.HandleKeyboard(data);
	}

	void ChatElement::Update(const float dt)
	{
		textbox.Update(dt);
	}

	void ChatElement::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		chatWindow.Draw(sb, pb);
		textbox.Draw(sb, pb);
	}
}
