#pragma once

#include <glad.h>
#include <glfw3.h>
#include <string>
#include "InputProcessor.h"
#include "SpriteText.h"

namespace Minigolf
{
	class Game
	{
	private:

		static const int DefaultWidth = 800;
		static const int DefaultHeight = 600;

		static const float TargetFramerate;

		static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void CharCallback(GLFWwindow* window, unsigned int codepoint);
		static void CursorCallback(GLFWwindow* window, double x, double y);
		static void ButtonCallback(GLFWwindow* window, int button, int action, int mods);
		static void ResizeCallback(GLFWwindow* window, int width, int height);

		GLFWwindow* window;

		float previousTime = 0;
		float accumulator = 0;

		void RegisterCallbacks() const;

	protected:

		explicit Game(const std::string& title);

		InputProcessor inputProcessor;

	public:

		virtual ~Game();
		virtual void Initialize() = 0;
		virtual void Update(float dt) = 0;
		virtual void Draw() = 0;

		// This function is required for static GLFW callback functions.
		InputProcessor& GetInputProcessor();

		void Run();
	};

	inline Game::~Game() = default;
}
