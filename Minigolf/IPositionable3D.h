#pragma once
#include <glm/detail/type_vec3.hpp>

namespace Minigolf
{
	class IPositionable3D
	{
		public:

		virtual ~IPositionable3D();
		virtual glm::vec3 Position() const = 0;
		virtual void Position(glm::vec3 position) = 0;
	};

	inline IPositionable3D::~IPositionable3D() = default;
}
