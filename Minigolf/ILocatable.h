#pragma once
#include <glm/vec2.hpp>

namespace Minigolf
{
	class ILocatable
	{
		public:

		virtual ~ILocatable();
		virtual glm::ivec2 Location() const = 0;
		virtual void Location(const glm::ivec2& location) = 0;
	};

	inline ILocatable::~ILocatable() = default;
}
