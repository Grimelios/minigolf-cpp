#include "TimerCollection.h"

namespace Minigolf
{
	void TimerCollection::Add(Timer& timer)
	{
		timer.SetTimerCollection(this);
		timers.Add(timer);
	}

	void TimerCollection::Remove(const Timer& timer)
	{
		timers.Remove(timer);
	}

	void TimerCollection::Update(float dt)
	{
		timers.ProcessChanges();
	}
}
