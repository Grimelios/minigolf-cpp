#pragma once
#include <array>

namespace Minigolf
{
	namespace ArrayUtilities
	{
		template<class T, size_t S>
		bool Contains(std::array<T, S> a, const T& value)
		{
			return std::find(std::begin(a), std::end(a), value) != std::end(a);
		}
	}
}
