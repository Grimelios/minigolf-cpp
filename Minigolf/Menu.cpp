#include "Menu.h"

namespace Minigolf
{
	void Menu::ProcessInput(const AggregateData& data)
	{
	}

	void Menu::Update(const float dt)
	{
		for (MenuItem& item : items)
		{
			item.Update(dt);
		}
	}

	void Menu::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		for (MenuItem& item : items)
		{
			item.Draw(sb, pb);
		}
	}
}
