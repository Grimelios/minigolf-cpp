#pragma once

namespace Minigolf::UIConstants
{
	static const int Padding = 4;
	static const int Spacing = 2;
}
