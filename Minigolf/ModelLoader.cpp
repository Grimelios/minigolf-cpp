#include "ModelLoader.h"
#include "FileUtilities.h"
#include "Paths.h"
#include "StringUtilities.h"
#include <fstream>
#include "VectorUtilities.h"
#include <glm/detail/func_geometric.inl>
#include "ContentCache.h"

namespace Minigolf
{
	std::unique_ptr<Model> ModelLoader::Load(const std::string& filename) const
	{
		std::ifstream stream(Paths::Models + filename + ".obj");
		std::string line;

		// The first two lines of .obj files aren't useful here.
		FileUtilities::SkipLines(stream, 2);
		std::getline(stream, line);

		std::vector<glm::vec3> vertices;
		std::vector<glm::vec2> texCoords;
		std::vector<glm::vec3> faceNormals;

		// .obj files list vertices, texture coordinates, face normals, then faces (with a few lines in between).
		do
		{
			vertices.push_back(ParseVec3(line));
			std::getline(stream, line);
		}
		while (line[1] != 't');

		do
		{
			texCoords.push_back(ParseVec2(line));
			std::getline(stream, line);
		}
		while (line[1] != 'n');

		do
		{
			faceNormals.push_back(ParseVec3(line));
			std::getline(stream, line);
		} 
		while (line[0] != 's');

		// When models are exported, all faces are triangulated. This can result in incorrect normal calculations for
		// vertices if a larger, non-triangular polygon is split (like a rectangle). Using this map allows vertices to
		// track the faces from which they've already pulled normals.
		std::vector<std::vector<unsigned int>> faceMap = std::vector<std::vector<unsigned int>>(vertices.size());
		std::vector<glm::vec3> vertexNormals = std::vector<glm::vec3>(vertices.size());
		std::vector<unsigned int> indices;

		while (std::getline(stream, line))
		{
			std::vector<std::string> tokens = StringUtilities::Split(line, ' ');

			for (int i = 1; i <= 3; i++)
			{
				std::vector<std::string> subTokens = StringUtilities::Split(tokens[i], '/');

				// .obj files use 1-indexing rather than 0.
				const unsigned int vertex = std::stoi(subTokens[0]) - 1;
				const unsigned int face = std::stoi(subTokens[2]) - 1;

				std::vector<unsigned int>& faces = faceMap[vertex];

				if (!VectorUtilities::Contains(faces, face))
				{
					vertexNormals[vertex] += faceNormals[face];
					faces.push_back(face);
				}

				indices.push_back(vertex);
			}
		}

		for (glm::vec3& n : vertexNormals)
		{
			n = normalize(n);
		}

		// For the time being, only the texture is applicable from the material file.
		std::ifstream materialStream(Paths::Models + filename + ".mtl");

		// The texture path is on the line starting with "map_Kd" (assumed at the end of the file).
		do
		{
			std::getline(materialStream, line);
		}
		while (line[0] != 'm');

		const std::string& textureFilename = line.substr(StringUtilities::LastIndexOf(line, '\\'));
		const GLuint textureId = ContentCache::GetTexture(textureFilename).TextureId();

		return std::make_unique<Model>(Model(vertices, vertexNormals, texCoords, indices, textureId));
	}

	glm::vec3 ModelLoader::ParseVec3(const std::string& value) const
	{
		std::vector<std::string> tokens = StringUtilities::Split(value, ' ');

		const float x = std::stof(tokens[1]);
		const float y = std::stof(tokens[2]);
		const float z = std::stof(tokens[3]);

		return glm::vec3(x, y, z);
	}

	glm::vec2 ModelLoader::ParseVec2(const std::string& value) const
	{
		std::vector<std::string> tokens = StringUtilities::Split(value, ' ');

		const float x = std::stof(tokens[1]);
		const float y = std::stof(tokens[2]);

		return glm::vec2(x, y);
	}
}
