#include "AggregateData.h"

namespace Minigolf
{
	AggregateData::AggregateData() : dataArray()
	{
		for (int i = 0; i < static_cast<int>(InputTypes::Count); i++)
		{
			dataArray[i] = nullptr;
		}
	}

	const InputData& AggregateData::Data(InputTypes type) const
	{
		return *dataArray[static_cast<int>(type)];
	}

	void AggregateData::Data(InputTypes type, const InputData& data)
	{
		dataArray[static_cast<int>(type)] = &data;
	}

	bool AggregateData::Query(const List<InputBind>& binds, const InputStates state) const
	{
		for (const InputBind& bind : binds)
		{
			if (dataArray[static_cast<int>(bind.Type())]->Query(bind.Data(), state))
			{
				return true;
			}
		}

		return false;
	}
}
