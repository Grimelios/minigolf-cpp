#include "SimpleComponent2D.h"

namespace Minigolf
{
	glm::ivec2 SimpleComponent2D::Location() const
	{
		return location;
	}

	glm::vec4 SimpleComponent2D::Color() const
	{
		return color;
	}

	void SimpleComponent2D::Location(const glm::ivec2& location)
	{
		this->location = location;
	}

	void SimpleComponent2D::Color(const glm::vec4& color)
	{
		this->color = color;
	}
}
