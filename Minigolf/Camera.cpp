#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>

namespace Minigolf
{
	Camera::Camera() : position(0), viewTarget(0), view(1), target(nullptr)
	{
		mode = CameraModes::Fixed;
	}

	glm::vec3 Camera::Position() const
	{
		return position;
	}

	const glm::mat4& Camera::View() const
	{
		return view;
	}

	void Camera::Position(const glm::vec3 position)
	{
		this->position = position;
	}

	void Camera::LookAt(const glm::vec3& position)
	{
		viewTarget = position;
	}

	void Camera::Target(const Entity& target)
	{
		this->target = &target;
	}

	void Camera::Mode(const CameraModes mode)
	{
		this->mode = mode;
	}

	void Camera::Update(const float dt)
	{
		switch (mode)
		{
			case CameraModes::Follow: UpdateFollow(dt); break;
			case CameraModes::Fly: UpdateFly(dt); break;
		}

		view = lookAt(position, viewTarget, glm::vec3(0, 1, 0));
	}

	void Camera::UpdateFollow(const float dt)
	{
		position = target->Position() + glm::vec3(0, 2, 4);
		viewTarget = target->Position();
	}

	void Camera::UpdateFly(const float dt)
	{
	}
}
