#pragma once
#include "Container2D.h"
#include "DoublyLinkedList.h"
#include "ChatMessage.h"
#include "UIConstants.h"

namespace Minigolf
{
	class ChatWindow : public Container2D
	{
	private:

		using ChatNode = DoublyLinkedListNode<ChatMessage>;

		std::string playerName;

		DoublyLinkedList<ChatMessage> messages;
		ChatNode* startNode = nullptr;
		ChatNode* endNode = nullptr;

		int padding = 0;
		int spacing = 0;
		int fontSize = 0;
		int lineCount = 0;

		glm::ivec2 chatBase = glm::ivec2(0);
		glm::ivec2 chatOffset = glm::ivec2(0);

	public:

		explicit ChatWindow(std::string playerName);

		void Add(const std::string& message);
		void Location(const glm::ivec2& location) override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
