#pragma once
#include <string>
#include <vector>
#include "SimpleComponent2D.h"

namespace Minigolf
{
	class ChatActiveMessage : public SimpleComponent2D
	{
	private:

		const SpriteFont& font;

		std::vector<std::string> lines;
		std::vector<glm::ivec2> locations;
		std::vector<int> widths;

		int nameOffset = 0;
		int lastSpace = -1;
		int lineIndex = 0;
		int targetWidth = 0;

		glm::ivec2 startingLocation;

	public:

		ChatActiveMessage();

		glm::ivec2 ComputeCursorPosition(int cursor);

		void Clear();
		void NameOffset(int offset);
		void TargetWidth(int targetWidth);
		void Value(const std::string& value);
		void Modify(int cursor, int removalCount, const std::string& s);
		void Location(const glm::ivec2& location) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
