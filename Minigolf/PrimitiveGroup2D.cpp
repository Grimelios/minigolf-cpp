#include "PrimitiveGroup2D.h"

namespace Minigolf
{
	PrimitiveGroup2D::PrimitiveGroup2D(const GLenum mode) : mode(mode)
	{
	}

	GLenum PrimitiveGroup2D::Mode() const
	{
		return mode;
	}

	std::vector<glm::vec3>& PrimitiveGroup2D::Vertices()
	{
		return vertices;
	}

	std::vector<glm::vec4>& PrimitiveGroup2D::Colors()
	{
		return colors;
	}

	std::vector<int>& PrimitiveGroup2D::Indices()
	{
		return indices;
	}

	int& PrimitiveGroup2D::IndexOffset()
	{
		return indexOffset;
	}

	bool PrimitiveGroup2D::IsEmpty() const
	{
		return vertices.empty();
	}

	void PrimitiveGroup2D::Clear()
	{
		vertices.clear();
		colors.clear();
		indices.clear();

		indexOffset = 0;
	}
}
