#include "InputProcessor.h"
#include "AggregateData.h"
#include "KeyboardData.h"
#include "MouseData.h"
#include "Messaging.h"

namespace Minigolf
{
	InputProcessor::InputProcessor() : keyArray(), buttonArray(), mousePosition(0), previousMousePosition(0)
	{
		for (InputStates& state : keyArray)
		{
			state = InputStates::Released;
		}
	}

	void InputProcessor::OnKeyPress(const int key, const int mods)
	{
		keyArray[key] = InputStates::PressedThisFrame;
		keyPresses.emplace_back(key, mods);
	}

	void InputProcessor::OnKeyRepeat(const int key, const int mods)
	{
		keyRepeats.emplace_back(key, mods);
	}

	void InputProcessor::OnKeyRelease(const int key)
	{
		keyArray[key] = InputStates::ReleasedThisFrame;
	}

	void InputProcessor::OnMouseButtonPress(const int button)
	{
		buttonArray[button] = InputStates::PressedThisFrame;
	}

	void InputProcessor::OnMouseButtonRelease(const int button)
	{
		buttonArray[button] = InputStates::ReleasedThisFrame;
	}

	void InputProcessor::OnMouseMove(const float x, const float y)
	{
		mousePosition.x = x;
		mousePosition.y = y;
	}

	void InputProcessor::Update(const float dt)
	{
		KeyboardData keyboardData = GetKeyboardData();
		AggregateData aggregateData;
		aggregateData.Data(InputTypes::Keyboard, keyboardData);
		aggregateData.Data(InputTypes::Mouse, MouseData(mousePosition, previousMousePosition, buttonArray));

		Messaging::Send(MessageTypes::Keyboard, &keyboardData, dt);
		Messaging::Send(MessageTypes::Input, &aggregateData, dt);

		for (InputStates& state : keyArray)
		{
			switch (state)
			{
				case InputStates::PressedThisFrame: state = InputStates::Held; break;
				case InputStates::ReleasedThisFrame: state = InputStates::Released; break;
			}
		}

		previousMousePosition = mousePosition;
		keyPresses.clear();
		keyRepeats.clear();
	}

	KeyboardData InputProcessor::GetKeyboardData() const
	{
		std::vector<int> keysDown;
		std::vector<int> keysReleasedThisFrame;

		// Keys pressed this frame are handled in the key callback function.
		for (int key = 0; key < GLFW_KEY_LAST; key++)
		{
			switch (keyArray[key])
			{
				case InputStates::Held:
				case InputStates::PressedThisFrame:
					keysDown.push_back(key);

					break;

				case InputStates::ReleasedThisFrame:
					keysReleasedThisFrame.push_back(key);

					break;
			}
		}

		return KeyboardData(keysDown, keyPresses, keysReleasedThisFrame, keyArray);
	}
}
