#include "MinigolfGame.h"
#include "GameplayLoop.h"
#include "LobbyLoop.h"
#include "Messaging.h"

namespace Minigolf
{
	MinigolfGame::MinigolfGame() : Game("Minigolf")
	{
		sb.SetOther(&pb);
		pb.SetOther(&sb);
	}

	void MinigolfGame::Initialize()
	{
		Messaging::Initialize();
		ChatMessage::Initialize();

		glClearColor(0, 0, 0, 1);

		gameLoop = std::unique_ptr<GameLoop>(std::make_unique<GameplayLoop>(camera, sb, pb));
		gameLoop->Initialize();

		glm::ivec2 screenDimensions = glm::ivec2(800, 600);

		Messaging::Send(MessageTypes::Resize, &screenDimensions);
	}

	void MinigolfGame::Update(const float dt)
	{
		inputProcessor.Update(dt);
		camera.Update(dt);
		gameLoop->Update(dt);
	}

	void MinigolfGame::Draw()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		gameLoop->Draw();
		sb.Flush();
		pb.Flush();
	}
}
