#pragma once
#include <string>

namespace Minigolf
{
	namespace Paths
	{
		const std::string Content = "Content/";
		const std::string Fonts = "Content/Fonts/";
		const std::string Json = "Content/Json/";
		const std::string Models = "Content/Models/";
		const std::string Properties = "Content/Properties/";
		const std::string Shaders = "Content/Shaders/";
		const std::string Textures = "Content/Textures/";
	}
}
