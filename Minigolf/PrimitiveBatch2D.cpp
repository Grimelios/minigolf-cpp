#include "PrimitiveBatch2D.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Messaging.h"
#include "GLUtilities.h"

namespace Minigolf
{
	PrimitiveBatch2D::PrimitiveBatch2D() :
		shader("Primitives2D"),
		lineGroup(GL_LINES),
		lineLoopGroup(GL_LINE_LOOP),
		program(shader.Program()),
		lineIndices({ 0, 1 }),
		lineLoopIndices({ 0, 1, 2, 3 })
	{
		glGenBuffers(1, &positionBuffer);
		glGenBuffers(1, &colorBuffer);
		glGenBuffers(1, &indexBuffer);
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(glm::vec3), nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
		glVertexAttribPointer(1, 4, GL_FLOAT, false, sizeof(glm::vec4), nullptr);

		for (GLuint i = 0; i < 2; i++)
		{
			glEnableVertexAttribArray(i);
		}

		Messaging::Subscribe(MessageTypes::Resize, [this](void* data, const float dt)
		{
			const glm::ivec2& halfDimensions = *static_cast<glm::ivec2*>(data) / 2;

			glm::mat4 mvp = scale(glm::mat4(1), glm::vec3(1.0f / halfDimensions.x, 1.0f / halfDimensions.y, 1));
			mvp = translate(mvp, glm::vec3(-halfDimensions, 0));

			glUseProgram(program);
			glUniformMatrix4fv(0, 1, GL_FALSE, value_ptr(mvp));
		});
	}

	void PrimitiveBatch2D::SetOther(SpriteBatch* sb)
	{
		this->sb = sb;
	}

	void PrimitiveBatch2D::DrawLine(const glm::vec2& start, const glm::vec2& end, const glm::vec4& color)
	{
		PushData(lineGroup, std::array<glm::vec2, 2>({ start, end }), color, lineIndices);
	}

	void PrimitiveBatch2D::DrawBounds(const Bounds& bounds, const glm::vec4& color)
	{
		const std::array<glm::vec2, 4> vertexArray
		{
			// Substracting one makes the top border of the box better line up.
			glm::vec2(bounds.Left() - 1, bounds.Top()),
			glm::vec2(bounds.Right(), bounds.Top()),
			glm::vec2(bounds.Right(), bounds.Bottom()),
			glm::vec2(bounds.Left(), bounds.Bottom())
		};

		PushData(lineLoopGroup, vertexArray, color, lineLoopIndices);
	}

	void PrimitiveBatch2D::Flush()
	{
		glUseProgram(program);
		glBindVertexArray(vao);

		Flush(lineGroup);
		Flush(lineLoopGroup);

		zValue = ZStart;
	}

	void PrimitiveBatch2D::Flush(PrimitiveGroup2D& group)
	{
		if (group.IsEmpty())
		{
			return;
		}

		GLUtilities::BufferData(positionBuffer, group.Vertices(), sizeof(glm::vec3));
		GLUtilities::BufferData(colorBuffer, group.Colors(), sizeof(glm::vec4));

		const std::vector<int>& indices = group.Indices();

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
		glDrawElements(group.Mode(), indices.size(), GL_UNSIGNED_INT, nullptr);

		group.Clear();
	}
}
