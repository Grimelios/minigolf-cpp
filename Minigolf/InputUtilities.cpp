#include "InputUtilities.h"

namespace Minigolf
{
	bool InputUtilities::CheckLock(LockKeys lock)
	{
		// See https://stackoverflow.com/questions/13905342/winapi-how-to-get-the-caps-lock-state.
		return (GetKeyState(static_cast<int>(lock)) & 0x0001) > 0;
	}
}
