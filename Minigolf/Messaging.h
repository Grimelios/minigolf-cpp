#pragma once
#include <functional>
#include <array>
#include <optional>

namespace Minigolf
{
	enum class MessageTypes
	{
		Keyboard = 0,
		Mouse = 1,
		Input = 2,
		Exit = 3,
		Resize = 4,
		PlayerName = 5,
		Count = 6
	};

	class Messaging
	{
	private:

		static const int TypeCount = static_cast<int>(MessageTypes::Count);

		using ReceiverFunction = std::function<void(void*, float)>;
		using ReceiverVector = std::vector<std::optional<ReceiverFunction>>;
		using ReceiverArray = std::array<ReceiverVector, TypeCount>;
		using IndexArray = std::array<int, TypeCount>;

		static ReceiverArray receiverArray;
		static IndexArray indexArray;

	public:

		static void Initialize();
		static int Subscribe(MessageTypes messageType, const ReceiverFunction& action);
		static void Unsubscribe(MessageTypes messageType, int index);
		static void Send(MessageTypes messageType, void* data, float dt = 0);
		static void ProcessChanges();
	};
}
