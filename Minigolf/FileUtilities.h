#pragma once
#include "List.h"
#include <string>
#include <fstream>

namespace Minigolf
{
	namespace FileUtilities
	{
		std::string ReadAllText(const std::string& filename);
		std::vector<std::string> ReadAllLines(const std::string& filename);

		void SkipLine(std::ifstream& stream);
		void SkipLines(std::ifstream& stream, int count);
	}
}
