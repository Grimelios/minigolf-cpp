#pragma once
#include <map>

namespace Minigolf
{
	template<class K, class V>
	class Dictionary
	{
		private:

		std::map<K, V> map;

		public:

		void Add(const K& key, const V& value);
		void Add(const K& key, V&& value);
		void Remove(const K& key);

		bool TryGetValue(const K& key, V*& value);
		bool IsEmpty() const;

		int Size() const;

		V& operator[](const K& key);
		const V& operator[](const K& key) const;
	};

	template <class K, class V>
	void Dictionary<K, V>::Add(const K& key, const V& value)
	{
		map.insert(std::pair<K, V>(key, value));
	}

	template <class K, class V>
	void Dictionary<K, V>::Add(const K& key, V&& value)
	{
		map.insert(std::pair<K, V>(key, std::forward<V>(value)));
	}

	template <class K, class V>
	void Dictionary<K, V>::Remove(const K& key)
	{
		map.erase(key);
	}

	template <class K, class V>
	bool Dictionary<K, V>::TryGetValue(const K& key, V*& value)
	{
		auto iterator = map.find(key);

		if (iterator == map.end())
		{
			return false;
		}

		value = &map[key];

		return true;
	}

	template <class K, class V>
	bool Dictionary<K, V>::IsEmpty() const
	{
		return map.empty();
	}

	template <class K, class V>
	int Dictionary<K, V>::Size() const
	{
		return map.size();
	}

	template <class K, class V>
	V& Dictionary<K, V>::operator[](const K& key)
	{
		// See https://stackoverflow.com/questions/695645/why-does-the-c-map-type-argument-require-an-empty-constructor-when-using.
		return map.find(key)->second;
	}

	template <class K, class V>
	const V& Dictionary<K, V>::operator[](const K& key) const
	{
		// See the comment in the other [] operator function.
		return map.find(key)->second;
	}
}
