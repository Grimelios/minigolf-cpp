#pragma once

namespace Minigolf
{
	enum class Alignments2D
	{
		Center = 0,
		Left = 1,
		Right = 2,
		Top = 4,
		Bottom = 8,
		Unassigned = -1
	};

	inline Alignments2D operator&(Alignments2D a1, Alignments2D a2)
	{
		return static_cast<Alignments2D>(static_cast<int>(a1) & static_cast<int>(a2));
	}

	inline Alignments2D operator|(Alignments2D a1, Alignments2D a2)
	{
		return static_cast<Alignments2D>(static_cast<int>(a1) | static_cast<int>(a2));
	}
}
