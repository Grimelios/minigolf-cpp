#include "Game.h"
#include "MinigolfGame.h"

int main(int argc, char* argv[])
{
	Minigolf::MinigolfGame game;
	game.Initialize();
	game.Run();

	return 0;
}
