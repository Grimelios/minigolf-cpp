#pragma once
#include <memory>
#include <string>

namespace Minigolf
{
	template<class T>
	class GenericLoader
	{
		public:

		virtual ~GenericLoader();
		virtual std::unique_ptr<T> Load(const std::string& filename) const = 0;
	};

	template <class T>
	GenericLoader<T>::~GenericLoader() = default;
}
