#pragma once
#include "UIElement.h"

namespace Minigolf
{
	class TimerDisplay : public UIElement
	{
		public:

		TimerDisplay();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
