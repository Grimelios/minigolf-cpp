#pragma once
#include <glm/detail/type_vec4.hpp>

namespace Minigolf
{
	class IColorable
	{
		public:

		virtual ~IColorable();
		virtual glm::vec4 Color() const = 0;
		virtual void Color(const glm::vec4& color) = 0;
	};

	inline IColorable::~IColorable() = default;
}
