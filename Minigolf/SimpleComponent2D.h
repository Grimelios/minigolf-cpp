#pragma once
#include "ILocatable.h"
#include "IRenderable2D.h"
#include "IColorable.h"

namespace Minigolf
{
	class SimpleComponent2D : public ILocatable, public IColorable, public IRenderable2D
	{
		protected:

		glm::ivec2 location = glm::ivec2(0);
		glm::vec4 color = glm::vec4(1);

		public:

		virtual ~SimpleComponent2D() = 0;

		glm::ivec2 Location() const override;
		glm::vec4 Color() const override;

		void Location(const glm::ivec2& location) override;
		void Color(const glm::vec4& color) override;
	};

	inline SimpleComponent2D::~SimpleComponent2D() = default;
}
