#pragma once

#include "Entity.h"

namespace Minigolf
{
	class GolfBall : public Entity
	{
		private:

		const Model& model;

		float rotation = 0;

		public:

		GolfBall();

		const Model& Model() const;

		void Update(float dt) override;
	};
}
