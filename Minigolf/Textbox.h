#pragma once
#include "InputControl.h"
#include "KeyboardData.h"
#include "TextboxRenderer.h"
#include "Event.h"

namespace Minigolf
{
	class TextboxRenderer;
	class Textbox : public InputControl
	{
	private:

		using SubmitFunction = std::function<void(const std::string&)>;

		static std::optional<char> GetCharacter(int key, bool shift, bool capsLock);
		static const std::array<char, 10> numericSpecials;

		// The value is intentionally made static so that insert status is maintained when clicking among different textboxes.
		static bool insertMode;

		std::string value;

		// Using a pointer seems easier than using shared pointers. That means renderers must be owned by the class that owns
		// the textbox.
		TextboxRenderer* renderer;
		SubmitFunction submitFunction;
		
		int cursorPosition = 0;

	public:

		// See https://stackoverflow.com/questions/888235/overriding-a-bases-overloaded-function-in-c.
		using Container2D::Location;

		Textbox(TextboxRenderer& renderer, SubmitFunction submitFunction);

		void Location(const glm::ivec2& location) override;
		void Value(const std::string& value);
		void Cursor(int cursor);
		void HandleKeyboard(const KeyboardData& data);
		void OnHover() override;
		void OnUnhover() override;
		void OnClick() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
