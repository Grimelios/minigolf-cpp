#pragma once
#include "List.h"

namespace Minigolf
{
	template<class T>
	class SafeList
	{
		private:

		typedef typename std::vector<T>::iterator iterator;
		typedef const typename std::vector<T>::const_iterator const_iterator;

		List<T> mainList;
		List<T> addList;
		List<T> removeList;

		public:

		void Add(const T& item);

		template<size_t S>
		void AddRange(const std::array<T, S>& range);
		void AddRange(const std::vector<T>& range);

		void Remove(const T& item);
		void RemoveAt(int index);
		void Clear();
		void ProcessChanges();

		int Size();

		T& operator[](int index);

		iterator begin();
		iterator end();

		const_iterator begin() const;
		const_iterator end() const;
	};

	template <class T>
	void SafeList<T>::Add(const T& item)
	{
		addList.Add(item);
	}

	template <class T>
	template <size_t S>
	void SafeList<T>::AddRange(const std::array<T, S>& range)
	{
		addList.AddRange(range);
	}

	template <class T>
	void SafeList<T>::AddRange(const std::vector<T>& range)
	{
		addList.AddRange(range);
	}

	template <class T>
	void SafeList<T>::Remove(const T& item)
	{
		removeList.Add(item);
	}

	template <class T>
	void SafeList<T>::RemoveAt(const int index)
	{
		removeList.Add(mainList[index]);
	}

	template <class T>
	void SafeList<T>::Clear()
	{
		addList.Clear();
		removeList.Clear();
		mainList.Clear();
	}

	template <class T>
	void SafeList<T>::ProcessChanges()
	{
		for (T& item : removeList)
		{
			mainList.Remove(item);
		}

		for (T& item : addList)
		{
			mainList.Add(item);
		}
	}

	template <class T>
	int SafeList<T>::Size()
	{
		return mainList.Size();
	}

	template <class T>
	T& SafeList<T>::operator[](const int index)
	{
		return mainList[index];
	}

	template <class T>
	typename SafeList<T>::iterator SafeList<T>::begin()
	{
		return mainList.begin();
	}

	template <class T>
	typename SafeList<T>::iterator SafeList<T>::end()
	{
		return mainList.end();
	}

	template <class T>
	typename SafeList<T>::const_iterator SafeList<T>::begin() const
	{
		return mainList.begin();
	}

	template <class T>
	typename SafeList<T>::const_iterator SafeList<T>::end() const
	{
		return mainList.end();
	}
}
