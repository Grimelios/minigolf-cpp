#pragma once
#include "Bounds.h"

namespace Minigolf
{
	class IBoundable
	{
		public:

		virtual ~IBoundable();
		virtual Bounds& GetBounds() = 0;
		virtual const Bounds& GetBounds() const = 0;
	};

	inline IBoundable::~IBoundable() = default;
}
