#pragma once
#include <exception>
#include <string>

namespace Minigolf
{
	class GlfwException : public std::exception
	{
		public:

		explicit GlfwException(const std::string& message);
	};
}
