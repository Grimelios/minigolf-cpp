#include "Model.h"
#include <array>
#include "GLUtilities.h"
#include <glm/detail/type_vec2.hpp>
#include "ContentCache.h"

namespace Minigolf
{
	Model::Model(const std::vector<glm::vec3>& vertices, const std::vector<glm::vec3>& normals,
		const std::vector<glm::vec2>& texCoords, const std::vector<unsigned int>& indices, const GLuint textureId) :
		textureId(textureId), indexCount(indices.size())
	{
		// The buffers are position, normal, texture coordinates, and index buffer (in that order).
		std::array<GLuint, 4> buffers { };

		glGenBuffers(4, &buffers[0]);
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		GLUtilities::InitializeBuffer(buffers[0], 0, vertices, GL_FLOAT, 3, sizeof(glm::vec3));
		GLUtilities::InitializeBuffer(buffers[1], 1, normals, GL_FLOAT, 3, sizeof(glm::vec3));
		GLUtilities::InitializeBuffer(buffers[2], 2, texCoords, GL_FLOAT, 2, sizeof(glm::vec2));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[3]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
	}

	GLuint Model::Vao() const
	{
		return vao;
	}

	GLuint Model::TextureId() const
	{
		return textureId;
	}

	unsigned Model::IndexCount() const
	{
		return indexCount;
	}
}
