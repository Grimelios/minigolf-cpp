#pragma once
#include "UIElement.h"

namespace Minigolf
{
	class HoleDisplay : public UIElement
	{
		public:

		HoleDisplay();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
