#pragma once
#include <glm/detail/type_vec3.hpp>

namespace Minigolf
{
	class IScalable3D
	{
		public:

		virtual ~IScalable3D() = 0;
		virtual glm::vec3 Scale() const = 0;
		virtual void Scale(glm::vec3 scale) = 0;
	};

	inline IScalable3D::~IScalable3D() = default;
}
