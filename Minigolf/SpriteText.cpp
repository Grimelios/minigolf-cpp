#include "SpriteText.h"
#include <utility>
#include "ContentCache.h"
#include "GameFunctions.h"

namespace Minigolf
{
	SpriteText::SpriteText(const std::string& font, const std::optional<std::string>&& value, const bool useKerning,
		const Alignments2D alignment) :
		SpriteText(ContentCache::GetFont(font, useKerning), value, alignment)
	{
	}

	SpriteText::SpriteText(const SpriteFont& font, const std::optional<std::string>& value, const Alignments2D alignment) : 
		Component2D(alignment), font(font)
	{
		Value(value);
	}

	glm::ivec2 SpriteText::Measure() const
	{
		if (!value.has_value())
		{
			return glm::ivec2(0);
		}

		return font.Measure(value.value());
	}

	void SpriteText::Value(const std::optional<std::string>& value)
	{
		this->value = value;

		if (value.has_value())
		{
			const glm::ivec2 dimensions = font.Measure(value.value());
			origin = GameFunctions::ComputeOrigin(dimensions.x, dimensions.y, alignment);
		}
	}

	void SpriteText::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		if (value.has_value())
		{
			sb.DrawString(font, value.value(), position, origin, rotation, color, scale);
		}
	}
}
