#pragma once
#include "GameLoop.h"

namespace Minigolf
{
	class SplashLoop : public GameLoop
	{
		public:

		void Initialize() override;
		void Update(float dt) override;
		void Draw() override;
	};
}
