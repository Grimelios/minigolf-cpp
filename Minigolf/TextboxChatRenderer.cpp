#include "TextboxChatRenderer.h"
#include "ContentCache.h"

namespace Minigolf
{
	TextboxChatRenderer::TextboxChatRenderer() :
		nameFont(ContentCache::GetFont("ChatBold", false)),
		playerName(nameFont)
	{
	}

	void TextboxChatRenderer::Location(const glm::ivec2& location)
	{
		playerName.Position(location);
		message.Location(location);

		// This assumes that the location of the chatbox will only be set once (or at least before any text is entered).
		cursor.Location(message.ComputeCursorPosition(0));
	}

	void TextboxChatRenderer::PlayerName(const std::string& name)
	{
		const std::string& fullValue = "[" + name + "]: ";

		playerName.Value(fullValue);
		message.NameOffset(nameFont.Measure(fullValue).x);
	}

	void TextboxChatRenderer::TargetWidth(const int targetWidth)
	{
		message.TargetWidth(targetWidth);
	}

	void TextboxChatRenderer::Clear()
	{
		message.Clear();
		cursor.Location(message.ComputeCursorPosition(0));
	}

	void TextboxChatRenderer::OnValue(const std::string& value)
	{
		message.Value(value);
	}

	void TextboxChatRenderer::OnModify(const int cursor, const int removalCount, const std::string& s)
	{
		message.Modify(cursor, removalCount, s);
		this->cursor.Stall();
	}

	void TextboxChatRenderer::OnCursor(const int cursor)
	{
		this->cursor.Location(message.ComputeCursorPosition(cursor));
	}

	void TextboxChatRenderer::OnInsert(const bool insert)
	{
	}

	void TextboxChatRenderer::OnHover()
	{
	}

	void TextboxChatRenderer::OnUnhover()
	{
	}

	void TextboxChatRenderer::OnClick()
	{
	}

	void TextboxChatRenderer::Update(const float dt)
	{
		cursor.Update(dt);
	}

	void TextboxChatRenderer::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		playerName.Draw(sb, pb);
		message.Draw(sb, pb);
		cursor.Draw(sb, pb);
	}
}
