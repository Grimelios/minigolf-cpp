#include "ItemNotFoundException.h"

Minigolf::ItemNotFoundException::ItemNotFoundException(const std::string& message) :
	exception(message.c_str())
{
}
