#include "GameplayLoop.h"

namespace Minigolf
{
	GameplayLoop::GameplayLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch2D& pb) :
		GameLoop(camera, sb, pb)
	{
	}

	void GameplayLoop::Initialize()
	{
	}

	void GameplayLoop::Update(const float dt)
	{
		hud.Update(dt);
	}

	void GameplayLoop::Draw()
	{
		hud.Draw(sb, pb);
	}
}
