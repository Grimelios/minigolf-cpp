#pragma once
#include "TextboxRenderer.h"
#include "SpriteText.h"
#include "ChatActiveMessage.h"
#include "ChatCursor.h"

namespace Minigolf
{
	class TextboxChatRenderer : public TextboxRenderer
	{
	private:

		const SpriteFont& nameFont;

		SpriteText playerName;
		ChatActiveMessage message;
		ChatCursor cursor;

	public:

		TextboxChatRenderer();

		void Location(const glm::ivec2& location) override;
		void PlayerName(const std::string& name);
		void TargetWidth(int targetWidth);
		void Clear() override;
		void OnValue(const std::string& value) override;
		void OnModify(int cursor, int removalCount, const std::string& s) override;
		void OnCursor(int cursor) override;
		void OnInsert(bool insert) override;
		void OnHover() override;
		void OnUnhover() override;
		void OnClick() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
