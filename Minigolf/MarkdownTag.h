#pragma once

namespace Minigolf
{
	enum class MarkdownTypes
	{
		Regular = 0,
		Italic = 1,
		Bold = 2,
		BoldItalic = 3,
		Underline = 4,
		Strikethrough = 8
	};

	class MarkdownTag
	{
	private:

		int start;
		int end;

		MarkdownTypes type;

	public:

		MarkdownTag(int start, MarkdownTypes type);
		MarkdownTag(int start, int end, MarkdownTypes type);

		int Start() const;
		int& End();

		MarkdownTypes Type() const;
	};
}
