#include "Textbox.h"
#include "StringUtilities.h"
#include "VectorUtilities.h"
#include "SpriteText.h"
#include <cctype>
#include <utility>
#include "InputUtilities.h"
#include "UIConstants.h"

namespace Minigolf
{
	const std::array<char, 10> Textbox::numericSpecials
	{
		')', '!', '@', '#', '$', '%', '^', '&', '*', '('
	};

	bool Textbox::insertMode;

	Textbox::Textbox(TextboxRenderer& renderer, SubmitFunction submitFunction) :
		renderer(&renderer),
		submitFunction(std::move(submitFunction))
	{
	}

	void Textbox::Location(const glm::ivec2& location)
	{
		renderer->Location(location + glm::ivec2(UIConstants::Padding));

		Container2D::Location(location);
	}

	void Textbox::Value(const std::string& value)
	{
		const bool changed = this->value != value;

		this->value = value;

		if (changed)
		{
			renderer->OnValue(value);
		}
	}

	void Textbox::Cursor(const int cursor)
	{
		const bool changed = cursorPosition != cursor;

		cursorPosition = cursor;

		if (changed)
		{
			renderer->OnCursor(cursor);
		}
	}

	void Textbox::HandleKeyboard(const KeyboardData& data)
	{
		std::vector<KeyPress> keysPressedThisFrame(data.KeysPressedThisFrame());

		const int keyCount = keysPressedThisFrame.size();

		if (keyCount == 0)
		{
			return;
		}

		for (int i = 0; i < keyCount; i++)
		{
			const int key = keysPressedThisFrame[i].Key();

			// Enter (submission) is checked before anything else. If enter is pressed on a frame, all other keys are
			// ignored. This is true even if the text field is blank.
			if (!value.empty() && (key == GLFW_KEY_ENTER || key == GLFW_KEY_KP_ENTER))
			{
				submitFunction(value);

				// See https://stackoverflow.com/questions/20457437/does-stdstringclear-reclaim-the-memory-associated-with-a-string.
				value.clear();
				value.shrink_to_fit();
				renderer->Clear();
				cursorPosition = 0;

				return;
			}
		}

		std::vector<bool> keysUsed(keyCount);

		const int currentSize = static_cast<int>(value.size());
		const std::string previousValue = value;

		const bool previousInsert = insertMode;
		const bool capsLock = InputUtilities::CheckLock(LockKeys::CapsLock);
		const bool numLock = InputUtilities::CheckLock(LockKeys::NumLock);

		bool home = false;
		bool end = false;
		bool left = false;
		bool right = false;

		int removalCount = 0;

		if (!numLock)
		{
			for (int i = 0; i < keyCount; i++)
			{
				switch (keysPressedThisFrame[i].Key())
				{
					case GLFW_KEY_KP_7: home = true; break;
					case GLFW_KEY_KP_1: end = true; break;
					case GLFW_KEY_KP_4: left = true; break;
					case GLFW_KEY_KP_6: right = true; break;
					case GLFW_KEY_KP_0: insertMode = !insertMode; break;
				}
			}
		}

		// Cursor changes are handled first (along with changes to insert mode). Note that it's possible to toggle insert on
		// and off (or vice versa) on a single frame if num lock is off.
		for (int i = 0; i < keyCount; i++)
		{
			switch (keysPressedThisFrame[i].Key())
			{
				case GLFW_KEY_HOME: home = true; break;
				case GLFW_KEY_END: end = true; break;
				case GLFW_KEY_LEFT: left = true; break;
				case GLFW_KEY_RIGHT: right = true; break;
				case GLFW_KEY_INSERT: insertMode = !insertMode;	break;
			}
		}

		const int previousCursor = cursorPosition;

		// I'm choosing to cancel home/end if they're pressed on the same frame. This is different behavior (I think) from
		// other editors (like Visual Studio).
		if (home ^ end)
		{
			cursorPosition = home ? 0 : currentSize;
		}

		if (left ^ right)
		{
			if (left)
			{
				cursorPosition = cursorPosition == 0 ? 0 : --cursorPosition;
			}
			else if (cursorPosition < currentSize)
			{
				cursorPosition++;
			}
		}

		// Backspace and delete are handled next.
		for (int i = 0; i < keyCount; i++)
		{
			const int key = keysPressedThisFrame[i].Key();

			if (key == GLFW_KEY_BACKSPACE)
			{
				// It's impossible for the cursor to be greater than zero for an empty string (i.e. checking the value itself
				// isn't required). Same for the delete key below.
				if (cursorPosition > 0)
				{
					cursorPosition--;
					removalCount++;
				}

				keysUsed[i] = true;
			}
			else if (key == GLFW_KEY_DELETE)
			{
				if (cursorPosition < static_cast<int>(value.size()))
				{
					removalCount++;
				}

				keysUsed[i] = true;
			}
		}

		if (removalCount == 1)
		{
			value.erase(cursorPosition);
		}
		else if (removalCount > 1)
		{
			value.erase(cursorPosition, removalCount);
		}

		if (previousInsert != insertMode)
		{
			renderer->OnInsert(insertMode);
		}

		std::string newString;
		
		// Actual characters are handled last.
		for (int i = 0; i < keyCount; i++)
		{
			if (keysUsed[i])
			{
				continue;
			}

			const KeyPress& keyPress = keysPressedThisFrame[i];
			const int key = keyPress.Key();
			const int mods = keyPress.Mods();
			const bool shift = (mods & GLFW_MOD_SHIFT) > 0;

			std::optional<char> character = GetCharacter(key, shift, capsLock);

			if (character.has_value())
			{
				newString.push_back(character.value());
			}
		}

		const bool newData = !newString.empty();

		if (removalCount > 0 || newData)
		{
			if (newData)
			{
				if (insertMode)
				{
				}
				else
				{
					value.insert(cursorPosition, newString);
				}
			}

			// Passing the cursor position as well as removal count means that consuming classes don't necessarily need to
			// store the cursor themselves.
			renderer->OnModify(cursorPosition, removalCount, newString);

			// The local cursor is intentionally updated last so that the correct cursor value can be used above.
			cursorPosition += newString.size();
			renderer->OnCursor(cursorPosition);
		}
		// This handles the case when the cursor moves, but no text changes.
		else if (previousCursor != cursorPosition)
		{
			renderer->OnCursor(cursorPosition);
		}
	}

	std::optional<char> Textbox::GetCharacter(const int key, const bool shift, const bool capsLock)
	{
		// See http://www.glfw.org/docs/3.0/group__keys.html (for this whole function).
		if (key >= GLFW_KEY_A && key <= GLFW_KEY_Z)
		{
			return (shift || capsLock) ? key : std::tolower(key);
		}

		if (key >= GLFW_KEY_0 && key <= GLFW_KEY_9)
		{
			return shift ? numericSpecials[key - GLFW_KEY_0] : key;
		}

		if (key == GLFW_KEY_SPACE)
		{
			return ' ';
		}

		switch (key)
		{
			case GLFW_KEY_COMMA: return shift ? '<' : ',';
			case GLFW_KEY_PERIOD: return shift ? '>' : '.';
			case GLFW_KEY_SLASH: return shift ? '?' : '/';
			case GLFW_KEY_SEMICOLON: return shift ? ':' : ';';
			case GLFW_KEY_APOSTROPHE: return shift ? '"' : '\'';
			case GLFW_KEY_LEFT_BRACKET: return shift ? '{' : '[';
			case GLFW_KEY_RIGHT_BRACKET: return shift ? '}' : ']';
			case GLFW_KEY_BACKSLASH: return shift ? '|' : '\\';
			case GLFW_KEY_MINUS: return shift ? '_' : '-';
			case GLFW_KEY_EQUAL: return shift ? '+' : '=';
			case GLFW_KEY_GRAVE_ACCENT: return shift ? '~' : '`';

			case GLFW_KEY_KP_ADD: return '+';
			case GLFW_KEY_KP_SUBTRACT: return '-';
			case GLFW_KEY_KP_MULTIPLY: return '*';
			case GLFW_KEY_KP_DIVIDE: return '/';
			case GLFW_KEY_KP_DECIMAL: return '.';
		}

		return std::nullopt;
	}

	void Textbox::OnHover()
	{
		renderer->OnHover();
	}

	void Textbox::OnUnhover()
	{
		renderer->OnUnhover();
	}

	void Textbox::OnClick()
	{
		renderer->OnClick();
	}

	void Textbox::Update(const float dt)
	{
		renderer->Update(dt);
	}

	void Textbox::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		renderer->Draw(sb, pb);
	}
}
