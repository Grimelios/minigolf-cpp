#include "Container2D.h"

namespace Minigolf
{
	Container2D::Container2D(const bool visible, const bool centered) :
		location(0), visible(visible), centered(centered)
	{
	}

	bool Container2D::Visible() const
	{
		return visible;
	}

	void Container2D::Visible(const bool visible)
	{
		this->visible = visible;
	}

	int Container2D::Width() const
	{
		return bounds.Width();
	}

	int Container2D::Height() const
	{
		return bounds.Height();
	}

	void Container2D::Width(const int width)
	{
		bounds.Width(width);
	}

	void Container2D::Height(const int height)
	{
		bounds.Height(height);
	}

	glm::ivec2 Container2D::Location() const
	{
		return location;
	}

	Bounds& Container2D::GetBounds()
	{
		return bounds;
	}

	const Bounds& Container2D::GetBounds() const
	{
		return bounds;
	}

	void Container2D::Location(const glm::ivec2& location)
	{
		this->location = location;

		if (centered)
		{
			bounds.Center(location);
		}
		else
		{
			bounds.Location(location);
		}
	}
}
