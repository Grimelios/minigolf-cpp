#include "ChatMessage.h"
#include "ContentCache.h"
#include "UIConstants.h"
#include "StringUtilities.h"

namespace Minigolf
{
	MarkdownFormatter ChatMessage::formatter;
	ChatMessage::FontArray ChatMessage::fontArray;

	void ChatMessage::Initialize()
	{
		// Using an initialization function is required because creating the array in the decleration above causes read
		// access violations.
		fontArray = FontArray
		{
			&ContentCache::GetFont("Chat", false),
			&ContentCache::GetFont("ChatItalic", false),
			&ContentCache::GetFont("ChatBold", false),
			&ContentCache::GetFont("ChatBoldItalic", false),
		};
	}

	ChatMessage::ChatMessage(const std::string& sender, const std::string& message) :
		sender(*fontArray[2], "[" + sender + "]: ")
	{
		std::vector<MarkdownToken> tokens = formatter.ComputeTokens(message);
		glm::ivec2 offset = glm::ivec2(this->sender.Measure().x, 0);

		const int targetWidth = 300;

		for (MarkdownToken& token : tokens)
		{
			const int fontIndex = static_cast<int>(token.Type()) & static_cast<int>(MarkdownTypes::BoldItalic);
			const SpriteFont& font = *fontArray[fontIndex];

			std::vector<std::string> lines = StringUtilities::WrapLines(font, token.Value(), targetWidth, offset.x);

			const int fontSize = font.Size();
			const int count = static_cast<int>(lines.size());

			for (int i = 0; i < count; i++)
			{
				if (i > 0)
				{
					offset.x = 0;
					offset.y += fontSize + UIConstants::Spacing;
					lineCount++;
				}

				const std::string& line = lines[i];

				// The first line can be empty if the previous cursor position was just about to wrap.
				if (!line.empty())
				{
					textList.emplace_back(font, line);
					offsets.push_back(offset);
				}
			}

			offset.x += font.Measure(lines[count - 1]).x;
		}
	}

	int ChatMessage::LineCount() const
	{
		return lineCount;
	}

	void ChatMessage::Location(const glm::ivec2& location)
	{
		sender.Position(location);

		for (int i = 0; i < static_cast<int>(textList.size()); i++)
		{
			textList[i].Position(location + offsets[i]);
		}

		Container2D::Location(location);
	}

	void ChatMessage::Update(float dt)
	{
	}

	void ChatMessage::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		sender.Draw(sb, pb);

		for (SpriteText& t : textList)
		{
			t.Draw(sb, pb);
		}
	}
}
