#pragma once
#include <windows.h>
#include <winuser.h>

namespace Minigolf
{
	enum class LockKeys : short
	{
		CapsLock = VK_CAPITAL,
		NumLock = VK_NUMLOCK,
		ScrollLock = VK_SCROLL
	};

	namespace InputUtilities
	{
		bool CheckLock(LockKeys lock);
	}
}
