#pragma once
#include "IBoundable.h"
#include "IDynamic.h"
#include "IRenderable2D.h"
#include "TimerCollection.h"
#include "ILocatable.h"

namespace Minigolf
{
	class Container2D : public ILocatable, public IBoundable, public IDynamic, public IRenderable2D
	{
	protected:

		glm::ivec2 location;
		Bounds bounds;

		bool visible;
		bool centered;

		TimerCollection timerCollection;

		explicit Container2D(bool visible = true, bool centered = false);

	public:

		virtual ~Container2D() = 0;

		glm::ivec2 Location() const override;
		Bounds& GetBounds() override;
		const Bounds& GetBounds() const override;

		bool Visible() const;
		int Width() const;
		int Height() const;

		void Location(const glm::ivec2& location) override;
		void Width(int width);
		void Height(int height);
		void Visible(bool visible);
	};

	inline Container2D::~Container2D() = default;
}
