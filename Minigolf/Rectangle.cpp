#include "Rectangle.h"

namespace Minigolf
{
	Rectangle::Rectangle() : Rectangle(0, 0, 0, 0)
	{
	}

	Rectangle::Rectangle(const float width, const float height) : Rectangle(0, 0, width, height)
	{
	}

	Rectangle::Rectangle(const float x, const float y, const float width, const float height) :
		x(x), y(y), width(width), height(height)
	{
	}

	float Rectangle::X() const
	{
		return x;
	}

	float Rectangle::Y() const
	{
		return y;
	}

	float Rectangle::Width() const
	{
		return width;
	}

	float Rectangle::Height() const
	{
		return height;
	}

	void Rectangle::X(const float x)
	{
		this->x = x;
	}

	void Rectangle::Y(const float y)
	{
		this->y = y;
	}

	void Rectangle::Width(const float width)
	{
		this->width = width;
	}

	void Rectangle::Height(const float height)
	{
		this->height = height;
	}

	glm::vec2 Rectangle::Position() const
	{
		return glm::vec2(x, y);
	}

	glm::vec2 Rectangle::Center() const
	{
		return glm::vec2(x + width / 2, y + height / 2);
	}

	void Rectangle::Position(const glm::vec2 point)
	{
		x = point.x;
		y = point.y;
	}

	void Rectangle::Center(const glm::vec2 center)
	{
		x = center.x - width / 2;
		y = center.y - height / 2;
	}
}
