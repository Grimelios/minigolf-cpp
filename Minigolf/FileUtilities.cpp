#include "FileUtilities.h"
#include <fstream>

namespace Minigolf
{
	std::string FileUtilities::ReadAllText(const std::string& filename)
	{
		// See https://stackoverflow.com/questions/195323/what-is-the-most-elegant-way-to-read-a-text-file-with-c/195350.
		std::ifstream stream(filename);

		return std::string(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());
	}

	std::vector<std::string> FileUtilities::ReadAllLines(const std::string& filename)
	{
		std::ifstream stream(filename);

		if (!stream.is_open())
		{			
		}

		std::vector<std::string> lines;
		std::string line;

		while (std::getline(stream, line))
		{
			lines.push_back(std::move(line));
		}

		return lines;
	}

	void FileUtilities::SkipLine(std::ifstream& stream)
	{
		stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}

	void FileUtilities::SkipLines(std::ifstream& stream, const int count)
	{
		for (int i = 0; i < count; i++)
		{
			// See https://stackoverflow.com/questions/477408/ifstream-end-of-line-and-move-to-next-line.
			stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}
}
