#pragma once
#include "Model.h"
#include <string>
#include "Dictionary.h"
#include "GenericLoader.h"

namespace Minigolf
{
	class ModelLoader : public GenericLoader<Model>
	{
		private:

		glm::vec2 ParseVec2(const std::string& value) const;
		glm::vec3 ParseVec3(const std::string& value) const;

		public:

		std::unique_ptr<Model> Load(const std::string& filename) const override;
	};
}
