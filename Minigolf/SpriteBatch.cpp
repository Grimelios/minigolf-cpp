#include "SpriteBatch.h"
#include "GLUtilities.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "VectorUtilities.h"
#include "Messaging.h"
#include "MapUtilities.h"

namespace Minigolf
{
	SpriteBatch::SpriteBatch() :
		spriteShader("Sprite"),
		quadTexCoords(
		{
			glm::vec2(0.0f),
			glm::vec2(1.0f, 0.0f),
			glm::vec2(1.0f),
			glm::vec2(0.0f, 1.0f)
		}),

		quadIndices(
		{
			0, 1, 2, 0, 2, 3
		}),

		program(spriteShader.Program())
	{
		glGenBuffers(1, &positionBuffer);
		glGenBuffers(1, &texCoordBuffer);
		glGenBuffers(1, &colorBuffer);
		glGenBuffers(1, &indexBuffer);
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordBuffer);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(glm::vec4), nullptr);

		for (GLuint i = 0; i < 3; i++)
		{
			glEnableVertexAttribArray(i);
		}

		MapUtilities::Add(spriteMap, &spriteShader, InnerMap());
		activeMap = &MapUtilities::Get(spriteMap, &spriteShader);

		Messaging::Subscribe(MessageTypes::Resize, [this](void* data, const float dt)
		{
			const glm::ivec2& halfDimensions = *static_cast<glm::ivec2*>(data) / 2;

			glm::mat4 mvp = scale(glm::mat4(1), glm::vec3(1.0f / halfDimensions.x, 1.0f / halfDimensions.y, 1));
			mvp = translate(mvp, glm::vec3(-halfDimensions, 0));

			glUseProgram(program);
			glUniformMatrix4fv(0, 1, GL_FALSE, value_ptr(mvp));
		});
	}

	void SpriteBatch::SetOther(PrimitiveBatch2D* pb)
	{
		this->pb = pb;
	}

	void SpriteBatch::Apply(const ShaderProgram& shader)
	{
	}

	void SpriteBatch::Draw(const Texture2D& texture, const std::optional<Rectangle>& sourceRect, glm::vec2 position,
		glm::ivec2 origin, const float rotation, const glm::vec4& color, const glm::vec2& scale)
	{
		VerifyTexture(texture);

		const glm::vec2 dimensions = glm::vec2(texture.Width(), texture.Height()) * scale;

		std::array<glm::vec2, 4> vertexArray
		{
			glm::vec2(0.0f),
			glm::vec2(dimensions.x, 0.0f),
			dimensions,
			glm::vec2(0.0f, dimensions.y)
		};

		origin.x = static_cast<int>(origin.x * scale.x);
		origin.y = static_cast<int>(origin.y * scale.y);

		for (glm::vec2& v : vertexArray)
		{
			v -= origin;
		}

		if (rotation != 0)
		{
			for (int i = 0; i < 4; i++)
			{
				const glm::mat4 rotationMatrix = rotate(glm::mat4(1.0f), rotation, glm::vec3(0, 0, 1));
				const glm::vec4 v = rotationMatrix * glm::vec4(vertexArray[i], 0, 1);

				vertexArray[i] = glm::vec2(v.x, v.y);
			}
		}

		// Rounding to integer values reduces blurriness.
		position.x = round(position.x);
		position.y = round(position.y);

		for (glm::vec2& v : vertexArray)
		{
			v += position;
		}

		std::array<glm::vec3, 4> vertices3D;

		for (int i = 0; i < 4; i++)
		{
			vertices3D[i] = glm::vec3(vertexArray[i], zValue);
		}

		VectorUtilities::PushArray(activeGroup->Vertices(), vertices3D);
		VectorUtilities::PushArray(activeGroup->TexCoords(), quadTexCoords);
		VectorUtilities::Repeat(activeGroup->Colors(), color, 4);

		std::vector<int>& indices = activeGroup->Indices();

		int& indexOffset = activeGroup->IndexOffset();

		for (int i = 0; i < 6; i++)
		{
			indices.push_back(indexOffset + quadIndices[i]);
		}

		indexOffset += 4;

		zValue -= ZIncrement;
		pb->ZValue(zValue);
	}

	void SpriteBatch::DrawString(const SpriteFont& font, const std::string& value, const glm::vec2& position,
		const glm::vec4& color)
	{
		DrawString(font, value, position, glm::ivec2(0), 0, color, glm::vec2(1));
	}

	void SpriteBatch::DrawString(const SpriteFont& font, const std::string& value, const glm::vec2& position,
		const glm::ivec2& origin, const float rotation, const glm::vec4& color, const glm::vec2& scale)
	{
		VerifyTexture(font.Texture());

		// Casting position to integer values reduces blurriness.
		glm::vec2 localPosition = -glm::vec2(origin);
		localPosition.x += static_cast<int>(position.x);
		localPosition.y += static_cast<int>(position.y);

		for (char c : value)
		{
			const CharacterData& data = font.Data(c);

			const int width = data.Width();
			const int height = data.Height();
			const int kerning = 0;

			const glm::vec2 start = localPosition + glm::vec2(data.Offset()) + glm::vec2(kerning, 0);
			const std::array<glm::vec2, 4> vertexArray =
			{
				start,
				start + glm::vec2(width, 0),
				start + glm::vec2(width, height),
				start + glm::vec2(0, height)
			};

			std::array<glm::vec3, 4> vertices3D;

			for (int i = 0; i < 4; i++)
			{
				vertices3D[i] = glm::vec3(vertexArray[i], zValue);
			}

			VectorUtilities::PushArray(activeGroup->Vertices(), vertices3D);
			VectorUtilities::PushArray(activeGroup->TexCoords(), data.TexCoords());

			std::vector<int>& indices = activeGroup->Indices();

			int& indexOffset = activeGroup->IndexOffset();

			// These groups of six indices could be pushed inside this loop or outside. Both options seem about the same.
			for (int i = 0; i < 6; i++)
			{
				indices.push_back(indexOffset + quadIndices[i]);
			}

			indexOffset += 4;

			zValue -= ZIncrement;
			localPosition.x += data.Advance();
		}

		VectorUtilities::Repeat(activeGroup->Colors(), color, value.length() * 4);
		
		zValue -= ZIncrement;
		pb->ZValue(zValue);
	}

	void SpriteBatch::VerifyTexture(const Texture2D& texture)
	{
		if (activeTexture == nullptr || activeTexture != &texture)
		{
			activeTexture = &texture;
			InnerMap& map = *activeMap;

			if (!MapUtilities::TryGetValue(map, activeTexture, activeGroup))
			{
				MapUtilities::Add(map, activeTexture, SpriteGroup());
				activeGroup = &MapUtilities::Get(map, activeTexture);
			}
		}
	}

	void SpriteBatch::Flush()
	{
		glBindVertexArray(vao);

		for (auto& pair : spriteMap)
		{
			glUseProgram(pair.first->Program());

			for (auto& innerPair : pair.second)
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, innerPair.first->TextureId());

				Flush(innerPair.second);
			}
		}

		activeTexture = nullptr;
		activeGroup = nullptr;
		activeMap = &MapUtilities::Get(spriteMap, &spriteShader);

		zValue = ZStart;
	}

	void SpriteBatch::Flush(SpriteGroup& group)
	{
		if (group.IsEmpty())
		{
			return;
		}

		GLUtilities::BufferData(positionBuffer, group.Vertices(), sizeof(glm::vec3));
		GLUtilities::BufferData(texCoordBuffer, group.TexCoords(), sizeof(glm::vec2));
		GLUtilities::BufferData(colorBuffer, group.Colors(), sizeof(glm::vec4));

		const std::vector<int>& indices = group.Indices();

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr);

		group.Clear();
	}
}
