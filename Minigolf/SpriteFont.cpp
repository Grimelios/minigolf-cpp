#include "SpriteFont.h"

namespace Minigolf
{
	SpriteFont::SpriteFont(const Texture2D& texture, const int size, const CharacterArray& dataArray) :
		texture(texture), dataArray(dataArray), size(size)
	{
	}

	const CharacterData& SpriteFont::Data(const int index) const
	{
		return dataArray[index].value();
	}

	const Texture2D& SpriteFont::Texture() const
	{
		return texture;
	}

	int SpriteFont::Size() const
	{
		return size;
	}

	glm::ivec2 SpriteFont::Measure(const std::string& value, const bool measureLiteral) const
	{
		if (value.empty())
		{
			return glm::ivec2(0, size);
		}

		int sumWidth = 0;
		const int length = static_cast<int>(value.size());

		for (int i = 0; i < length - 1; i++)
		{
			char c = value[i];
			sumWidth += dataArray[value[i]]->Advance();
		}

		// This assumes the measured string has at least one character.
		char c = value[length - 1];
		sumWidth += dataArray[value[length - 1]]->Width();

		return glm::ivec2(sumWidth, size);
	}
}
