#pragma once
#include "TextureLoader.h"
#include "ModelLoader.h"
#include "FontLoader.h"
#include "SpriteFont.h"
#include "Paths.h"

namespace Minigolf
{
	class SpriteFont;
	class ContentCache
	{
		private:

		using FontCache = std::map<std::string, SpriteFont>;
		typedef Dictionary<std::string, std::unique_ptr<Model>> ModelCache;
		typedef Dictionary<std::string, std::unique_ptr<Texture2D>> TextureCache;

		static FontCache fontCache;
		static FontLoader fontLoader;
		static ModelCache modelCache;
		static ModelLoader modelLoader;
		static TextureCache textureCache;
		static TextureLoader textureLoader;

		template<class T>
		static T& GetGeneric(const std::string& filename, Dictionary<std::string, std::unique_ptr<T>>& cache,
			const GenericLoader<T>& loader);

		public:

		static Model& GetModel(const std::string& filename);
		static const SpriteFont& GetFont(const std::string& name, bool useKerning = true);
		static Texture2D& GetTexture(const std::string& filename, const std::string& folder = Paths::Textures);
	};

	template<class T>
	T& ContentCache::GetGeneric(const std::string& filename, Dictionary<std::string, std::unique_ptr<T>>& cache,
		const GenericLoader<T>& loader)
	{
		std::unique_ptr<T>* pointer;

		if (cache.TryGetValue(filename, pointer))
		{
			return **pointer;
		}

		std::unique_ptr<T> unique = loader.Load(filename);

		T& returnValue = *unique;

		cache.Add(filename, std::move(unique));

		return returnValue;
	}
}
