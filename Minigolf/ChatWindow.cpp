#include "ChatWindow.h"
#include "ContentCache.h"
#include "Properties.h"

namespace Minigolf
{
	ChatWindow::ChatWindow(std::string playerName) :
		playerName(std::move(playerName)),
		fontSize(ContentCache::GetFont("Chat").Size())
	{
		Properties::PropertyMap& map = Properties::Load("UI.properties");

		padding = std::stoi(map.find("Chat.Window.Padding")->second);
		spacing = std::stoi(map.find("Chat.Window.Spacing")->second);
		lineCount = std::stoi(map.find("Chat.Window.Lines")->second);

		const int width = std::stoi(map.find("Chat.Window.Width")->second);
		const int height = lineCount * (fontSize + spacing) - spacing + padding * 2;

		bounds.Width(width);
		bounds.Height(height);
	}

	void ChatWindow::Add(const std::string& message)
	{
		ChatMessage chatMessage(playerName, message);

		const int combined = fontSize + spacing;
		int messageHeight = chatMessage.LineCount() * combined;

		// The message must be added first in order to set start and end nodes below.
		messages.Add(chatMessage);

		// The first message posted to the chat window can subtract top spacing, since it doesn't need to push away any
		// existing messages.
		if (messages.Count() == 1)
		{
			messageHeight -= spacing;
			startNode = messages.Head();
			endNode = startNode;
		}
		else
		{
			endNode = messages.Tail();
		}

		chatOffset.y += messageHeight;

		ChatNode* node = messages.Head();

		glm::ivec2 location = chatBase - chatOffset;

		do
		{
			ChatMessage* currentMessage = &node->Data();
			currentMessage->Location(location);
			location.y += currentMessage->LineCount() * combined;
			node = node->Next();
		}
		while (node != nullptr);
	}

	void ChatWindow::Location(const glm::ivec2& location)
	{
		chatBase = location + glm::ivec2(padding, bounds.Height() - padding);

		Container2D::Location(location);
	}

	void ChatWindow::Update(const float dt)
	{
	}

	void ChatWindow::Draw(SpriteBatch& sb, PrimitiveBatch2D& pb)
	{
		pb.DrawBounds(bounds, glm::vec4(0, 1, 0, 1));

		if (startNode == nullptr)
		{
			return;
		}

		ChatNode* node = startNode;
		ChatNode* end = endNode->Next();

		do
		{
			node->Data().Draw(sb, pb);
			node = node->Next();
		}
		while (node != end);
	}
}
