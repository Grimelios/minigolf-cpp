#pragma once
#include "GameLoop.h"

namespace Minigolf
{
	class TitleLoop : public GameLoop
	{
		public:

		void Initialize() override;
		void Update(float dt) override;
		void Draw() override;
	};
}
