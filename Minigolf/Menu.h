#pragma once
#include "Container2D.h"
#include "MenuItem.h"
#include "VectorUtilities.h"
#include "AggregateData.h"

namespace Minigolf
{
	class MenuItem;
	class Menu : public Container2D
	{
		private:

		std::vector<MenuItem> items;

		void ProcessInput(const AggregateData& data);

		protected:

		template<size_t S>
		void Initialize(std::array<MenuItem, S> items);

		public:

		virtual ~Menu() = 0;

		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};

	inline Menu::~Menu() = default;

	template <size_t S>
	void Menu::Initialize(std::array<MenuItem, S> items)
	{
		VectorUtilities::PushArray(this->items, items);
	}
}
