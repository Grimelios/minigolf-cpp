#include "Texture2D.h"

namespace Minigolf
{
	Texture2D::Texture2D(const GLuint id, const int width, const int height) :
		width(width), height(height), textureId(id)
	{
	}

	int Texture2D::Width() const
	{
		return width;
	}

	int Texture2D::Height() const
	{
		return height;
	}

	GLuint Texture2D::TextureId() const
	{
		return textureId;
	}
}
