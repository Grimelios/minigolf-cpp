#pragma once
#include <vector>
#include "GraphNode.h"
#include "VerletPoint.h"

namespace Minigolf
{
	class FlagBuilder
	{
		using VerletList = std::vector<GraphNode<VerletPoint, float>>;

		public:

		static VerletList CreateRectangularFlag(int width, int height);
		static VerletList CreateTriangularFlag(int width, int height);
		static VerletList CreateBannerFlag(int width, int height);
	};
}
