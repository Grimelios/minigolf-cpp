#pragma once
#include "Container2D.h"
#include "SpriteText.h"

namespace Minigolf
{
	class PlayerLine : public Container2D
	{
		private:

		SpriteText playerName;

		public:

		PlayerLine(const std::string& playerName);

		void Color(const glm::vec3& color);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch2D& pb) override;
	};
}
